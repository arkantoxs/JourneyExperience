create table Administradores
(
  Dni varchar2(9) not null,
  clave varchar2(10)  not null UNIQUE,
  usuario varchar2(10)  not null UNIQUE,
  Nombre varchar2(50)  constraint nombre_Nn not null,
  Apellido varchar2(50)  constraint Apellido_Nn not null,
  dni_superusuario varchar2(9),
  
  constraint DniAdmin_pk primary key (Dni)
  
);

  alter table Administradores add foreign key (dni_superusuario) references Administradores (Dni);


create table Usuarios
(
  Usuario varchar2(20)constraint usuarioUsuario_Nn not null,
  Clave varchar(10) constraint claveUsuario_Nn not null,
  Nombre varchar2(50) constraint NombreUsuario_Nn not null,
  Apellido varchar2(50) constraint ApellidoUsuario_Nn not null,
  Email varchar(100) constraint EmailUsuario_Nn not null,
  Codigo_postal int,
  Ultimo_viaje varchar2(200),
  Pais_destino varchar2 (300),
  Estado varchar2(2),
  Otros_datos varchar2(300),
  
  constraint usuari_pk primary key (Usuario,Clave),
  constraint claveUsuario_Uk unique (clave,usuario)
);




create table Paises
(
  Nombre varchar2(200) constraint NombrePais_Nn not null,
  Moneda varchar2(200) constraint MonedaPais_Nn not null,
  Idioma varchar2(200) constraint IdiomaPais_Nn not null,
  Bandera varchar2(200) constraint BanderaPais_Uk unique,
  Capital varchar2(200) constraint CapitalPais_Nn not null,
  Habitantes int constraint HabitantePais_Nn not null,
  Nombre_cont varchar2(200)constraint ContinentePais_Nn not null,
  Dni_admin varchar2(9) not null,

  constraint nom_pk primary key (Nombre),
  constraint DniAdminpais_fk foreign key (Dni_admin) references Administradores(Dni)

);

/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Ciudades
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;


create table Ciudades_Pueblos
(
  id_ciudad int,
  Nombre varchar2(200)constraint NombreCiudad_Nn not null,
  descripcion varchar2(300),
  Nombre_pais varchar2(200),
  Dni_admin varchar2(9) not null,
  
     constraint nomPa_pk primary key (id_ciudad),
    constraint NombrePais_fk foreign key (Nombre_pais) references Paises(Nombre),
    constraint DniAdminCiudad_fk foreign key (Dni_admin) references Administradores(Dni)

);


/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Hoteles
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

Create table Hoteles
(
    Id_hotel int,
    Nombre varchar2(250) constraint NombreHotel_Nn not null,
    Contacto int constraint ContacteHotel_Nn not null, 
    Servicios varchar2(200) constraint ServiciosHotel_Nn not null, 
    Tarifas varchar2(100), 
    Descripcion varchar2(200) constraint DescripcionHotel_Nn not null,
    constraint hotel_pk primary key(Id_hotel)
);

Create table Tener_hotel
(
  Id_hotel int,
  id_ciudad int,
  Nombre_pais varchar2(200),
  Dni_admin varchar2(9) not null,
  
   constraint hot_fk1 foreign key (Id_hotel) references Hoteles (Id_hotel),
   constraint ciudad_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad),
   constraint NombrePaisHotel_fk foreign key (Nombre_pais) references Paises(Nombre),
   constraint DniAdminCiudadHotel_fk foreign key (Dni_admin) references Administradores(Dni)
);

  alter table Tener_hotel add constraint hot_ciudad_pk primary key (Id_hotel,id_ciudad);
  
CREATE SEQUENCE SEQ_Opinar_Hoteles
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000; 

create table Opinar_hotel
(
  
  Id_OpinarHotel int,
  Id_hotel int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300) constraint opinarHotel_Nn not null,
  data_comentario date,
  
  constraint hotelw_pk primary key(Id_Opinarhotel),
  constraint hotel_userr_fk1 foreign key (Id_hotel) references Hoteles (Id_hotel),
  constraint opinarUsuarioo_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
 /*alter table Opinar_hotel add constraint op_hot_user_pk primary key (Id_hotel,Clave,Usuario);*/


/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_restaurantes
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

Create table Restaurantes
(
  Id_restaurante int,
  Nombre varchar2(200) constraint NombreRestaurante_Nn not null,
  Contacto int constraint ContactoRestaurante_Nn not null,
  Especialidad varchar2(200) constraint EspecialidadRestaurante_Nn not null,
  Tarifas varchar2(200), 
  Horario varchar2(200) constraint HorarioRestaurante_Nn not null, 
  Descripcion varchar2(200) constraint DescripcionrRestaurante_Nn not null,
  
  
  constraint res_pk primary key(Id_restaurante)
);

 
create table Tener_restaurante
(
  Id_restaurante int,
  id_ciudad int,
  Nombre_pais varchar2(200),
  Dni_admin varchar2(9) not null,
  

 
   constraint NombrePaisResta_fk foreign key (Nombre_pais) references Paises(Nombre),
    constraint DniAdminCiudadResta_fk foreign key (Dni_admin) references Administradores(Dni),
  constraint res_user_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint ciudad_resta_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_restaurante add constraint entre_ciudad_pk primary key (Id_restaurante,id_ciudad);
  
CREATE SEQUENCE SEQ_Opinar_Resta
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000; 


  create table Opinar_restuarante
(
  id_OpinarResta int,
  Id_restaurante int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300) constraint OpinarRestaurante_Nn not null,
  data_comentario date,
  
    constraint hotelp_pk primary key(id_OpinarResta),
  constraint res_user_op1_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint opinar_Usuario_rest1_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
/*  alter table Opinar_restuarante add constraint op_resta_user_pk primary key ( Id_restaurante,Clave,Usuario);*/

/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Bares
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

create table Bares
(
  Id_bar int,
  Nombre varchar2(100)constraint NombreBar_Nn not null,
  Contacto int constraint ContactoBar_Nn not null,
  Especialidad varchar2(100) constraint EspecialidadBar_Nn not null, 
  Precio int constraint precioBar_Nn not null,
  Horario varchar2(50) constraint HorarioBar_Nn not null,
  Descripcion varchar2(200)constraint DescripcionBar_Nn not null,

 
  constraint barPais_pk primary key (Id_bar)
);

create table Tener_bar
(
  Id_bar int,
  id_ciudad int,
  Nombre_pais varchar2(200),
  Dni_admin varchar2(9) not null,
  

 
   constraint NombrePaisBar_fk foreign key (Nombre_pais) references Paises(Nombre),
   constraint DniAdminCiudadBar_fk foreign key (Dni_admin) references Administradores(Dni),
   constraint bares_user_op_fk1 foreign key (Id_bar) references Bares (Id_bar),
   constraint ciudad_bar_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);
  alter table Tener_bar add constraint bar_ciudad_pk primary key (Id_bar,id_ciudad);
  
  CREATE SEQUENCE SEQ_Opinar_Bar
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000; 

  create table Opinar_bar
(
  Id_OpinarBar int,
  Id_bar int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint hotele_pk primary key(Id_OpinarBar),
  constraint bare_fk1 foreign key (Id_bar) references Bares (Id_bar),
  constraint opinarUsuarioe_bar_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
 /* alter table Opinar_bar add constraint op_bar_user_pk primary key ( Id_bar,Clave,Usuario);*/
  
/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Entretenimiento
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

Create table Entretenimiento
(
  Id_entre int,
  Tipo varchar2(200) constraint TipoEnter_Nn not null,
  Descripcion varchar2(200) constraint descripcionEnter_Nn not null, 
  Horario varchar2(200) constraint horarioEnter_Nn not null,
  Contacto int constraint ContactoEnter_Nn not null,
 

  constraint entret_pk primary key(Id_entre)
);

create table Tener_entretenimiento
(
  Id_entre int,
  id_ciudad int,
  Nombre_pais varchar2(200),
  Dni_admin varchar2(9) not null,
  

 
   constraint NombrePaisEnte_fk foreign key (Nombre_pais) references Paises(Nombre),
    constraint DniAdminCiudadEntre_fk foreign key (Dni_admin) references Administradores(Dni),
  constraint enter_user_op_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint ciudad_entre_op_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_entretenimiento add constraint entre_2ciudad_pk primary key (Id_entre,id_ciudad);
  
  
 CREATE SEQUENCE SEQ_Opinar_entre
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000; 

 create table Opinar_entretenimiento
(
  id_OpinarEntre int,
  Id_entre int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
    constraint hote1l_pk primary key(id_OpinarEntre),

  constraint enter1_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint opinarUsuario1_entre_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
 /* alter table Opinar_entretenimiento add constraint op_entre_user_pk primary key ( Id_entre,Clave,Usuario);*/
  
CREATE SEQUENCE SEQ_servicios
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

create table Servicios 
(
  Id_servicio int,
  Tipo varchar2(200) constraint TipoServicio_Nn not null,
  descripcion varchar2(300) constraint DescripcionServicio_Nn not null,
  Contacto int constraint ContactoServicio_Nn not null,
  horario varchar2(100) constraint HorarioServicio_Nn not null,

  
  constraint servicio_pk primary key(Id_servicio)
);

create table Tener_servicios
(
  id_servicio int,
  id_ciudad int,
  Nombre_pais varchar2(200),
  Dni_admin varchar2(9) not null,
  

 
    constraint NombrePaisServicio_fk foreign key (Nombre_pais) references Paises(Nombre),
    constraint DniAdminCiudadServiciofk foreign key (Dni_admin) references Administradores(Dni),
    constraint serv_user_fk1 foreign key (id_servicio) references Servicios (Id_servicio),
    constraint ciudad_ser_op_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);
  alter table Tener_servicios add constraint serv_ciudad_pk primary key (Id_servicio,id_ciudad);

CREATE SEQUENCE SEQ_OpinarServicio
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

 create table Opinar_servicios
(
  id_OpinarServicio int,
  Id_servicio,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
     constraint hotel2_pk primary key(id_OpinarServicio),

  constraint serv_user_op2_fk1 foreign key (Id_servicio) references Servicios (Id_servicio),
  constraint opinarUsuario_serv2_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
 /* alter table Opinar_servicios add constraint op_serv_user_pk primary key ( Id_servicio,Clave,Usuario);*/