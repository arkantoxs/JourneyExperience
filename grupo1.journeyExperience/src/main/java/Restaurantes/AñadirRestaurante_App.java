package Restaurantes;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Pais.EliminarPaisApp;
import WipAssiya.SQL;
import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.JEditorPane;

public class AñadirRestaurante_App extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JTextField contacto;
	private JTextField tarifa;
	private JTextArea ColorBandera; 
	SQL bd=new SQL();
	private JTextField txtHorario;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A�adirHotel_App frame = new A�adirHotel_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public AñadirRestaurante_App() {
		setResizable(false);
		setTitle("Pais");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 589, 698);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(71, 47, 364, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(AñadirRestaurante_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(398, 30, 160, 76);
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Datos del restaurante");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel.setBounds(38, 222, 165, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(34, 275, 67, 14);
		contentPane.add(lblNewLabel_1);
		
		nombre = new JTextField();
		nombre.setBounds(139, 272, 121, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		contacto = new JTextField();
		contacto.setFont(new Font("Arial", Font.BOLD, 11));
		contacto.setColumns(10);
		contacto.setBounds(139, 303, 121, 20);
		contentPane.add(contacto);
		
		JLabel lblContinente = new JLabel("Pais");
		lblContinente.setFont(new Font("Arial", Font.BOLD, 11));
		lblContinente.setBounds(38, 331, 63, 14);
		contentPane.add(lblContinente);
		
		JLabel lblServicios = new JLabel("Especialidad");
		lblServicios.setFont(new Font("Arial", Font.BOLD, 11));
		lblServicios.setBounds(38, 456, 78, 14);
		contentPane.add(lblServicios);
		
		JLabel lblMoneda = new JLabel("Tarifas");
		lblMoneda.setFont(new Font("Arial", Font.BOLD, 11));
		lblMoneda.setBounds(34, 393, 67, 14);
		contentPane.add(lblMoneda);
		
		tarifa = new JTextField();
		tarifa.setColumns(10);
		tarifa.setBounds(139, 390, 121, 20);
		contentPane.add(tarifa);
		
		JLabel lblBandera = new JLabel("Descripcion");
		lblBandera.setFont(new Font("Arial", Font.BOLD, 11));
		lblBandera.setBounds(38, 516, 78, 14);
		contentPane.add(lblBandera);
		
		JButton btnNewButton_1 = new JButton("Guardar");
		btnNewButton_1.setFont(new Font("Arial", Font.BOLD, 11));
	
		btnNewButton_1.setBounds(445, 588, 89, 23);
		contentPane.add(btnNewButton_1);
		JLabel lblAdministrador = new JLabel("Administrador");
		lblAdministrador.setFont(new Font("Arial", Font.BOLD, 11));
		lblAdministrador.setBounds(291, 192, 97, 14);
		contentPane.add(lblAdministrador);
		
		final JLabel ValorHabitants = new JLabel("El valor debe ser num\u00E9rico");
		ValorHabitants.setForeground(Color.RED);
		ValorHabitants.setFont(new Font("Arial", Font.BOLD, 12));
		ValorHabitants.setBounds(270, 308, 165, 14);
		contentPane.add(ValorHabitants);
		ValorHabitants.setVisible(false);

		final JLabel DatosObligatorios = new JLabel("Todos los datos son obligatorios");
		DatosObligatorios.setForeground(Color.RED);
		DatosObligatorios.setFont(new Font("Arial", Font.BOLD, 12));
		DatosObligatorios.setBounds(165, 597, 232, 14);
		contentPane.add(DatosObligatorios);
		DatosObligatorios.setVisible(false);
		
		final JTextArea descripcion = new JTextArea();
		descripcion.setBounds(139, 500, 249, 71);
		contentPane.add(descripcion);
		
		JLabel label_3 = new JLabel("Contacto");
		label_3.setFont(new Font("Arial", Font.BOLD, 11));
		label_3.setBounds(38, 309, 67, 14);
		contentPane.add(label_3);
		
		final JEditorPane especialidad = new JEditorPane();
		especialidad.setBounds(139, 444, 249, 45);
		contentPane.add(especialidad);
		
		final JComboBox ListaCiudad = new JComboBox();
		ListaCiudad.setBounds(139, 359, 121, 20);
		contentPane.add(ListaCiudad);
		
		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setFont(new Font("Arial", Font.BOLD, 11));
		lblCiudad.setBounds(38, 362, 63, 14);
		contentPane.add(lblCiudad);
		
		final JComboBox Pais = new JComboBox();
		Pais.setBounds(139, 328, 121, 20);
		contentPane.add(Pais);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(AñadirRestaurante_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(38, 613, 68, 45);
		contentPane.add(btnAtras);
		
		final JComboBox ListaAdmin = new JComboBox();
		ListaAdmin.setBounds(384, 189, 121, 20);
		contentPane.add(ListaAdmin);
		
		JLabel lblHorario = new JLabel("Horario");
		lblHorario.setFont(new Font("Arial", Font.BOLD, 11));
		lblHorario.setBounds(38, 419, 67, 14);
		contentPane.add(lblHorario);
		
		txtHorario = new JTextField();
		txtHorario.setBounds(139, 416, 121, 20);
		contentPane.add(txtHorario);
		txtHorario.setColumns(10);
		
	//........................................ codigo para obtener todos los paises de BD.................................
		try {
			bd.SQLConnection("grupo1","root","");
			String rellenar[]=bd.getValuesPais();
		
				for(int i=0;i<rellenar.length;i++)
			{
			Pais.addItem(rellenar[i]);		
			}
			//bd.closeConnection();
	    } catch (Exception e2) {
		// TODO Auto-generated catch block
	    	e2.printStackTrace();
	    }
		
//........................................ FIN codigo para obtener todos los paises de BD.................................
		
	
//......................obtener los nombre de los admin. para identificar quien admin regisro el hotel....................
	
	
		String rellenarAdmin[]=bd.getValues_usuaroAdministrador();
		  
		
		for(int i=0;i<rellenarAdmin.length;i++)
		{
			ListaAdmin.addItem(rellenarAdmin[i]);
			
		}
//-------------------------------------------------------FIN CODIGO DE OBTENER admin.........................................		

//......................obtener los nombre de las ciudades. k estan en BD....................
		
			try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesCiudad();
				
				for(int i=0;i<rellenar.length;i++)
				{
					ListaCiudad.addItem(rellenar[i]);
					
				}
				bd.closeConnection();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	//-------------------------------------------------------FIN CODIGO DE OBTENER ciudad.........................................		

		
			
		

//..................................................evento para guardar los datos...........................................//

//..................................................evento para a�adir restaurante a bd...........................................//

		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
			  try {
				bd.SQLConnection("grupo1","root","");
			
	           String pais=Pais.getSelectedItem().toString();
	            String dniAdmin=ListaAdmin.getSelectedItem().toString();
			    String nom_ciudad= ListaCiudad.getSelectedItem().toString();
	          
		      int Contacto=Integer.parseInt(contacto.getText()); // pasamos contacto a int
	            
		          String dni=bd.Obtener_Dni_Admin(dniAdmin); //obtener el dni de admin
				int cod_ciudad=bd.ObtenerCod_Ciudad(nom_ciudad); // obtener el codigo de ciudad


	            //SQL bd=new SQL();
				
				 bd.insertarRestaurante(nombre.getText().toString(), Contacto, especialidad.getText().toString(),tarifa.getText().toString(),txtHorario.getText().toString(),descripcion.getText().toString());
				 int codRestaurante=bd.Obtener_IdRestaurante(nombre.getText().toString(), Contacto, especialidad.getText().toString(),tarifa.getText().toString(),descripcion.getText().toString());	
				 bd.insert_TenerHotel_Bar_Servicio_Resta_Entre("Tener_restaurante",codRestaurante,cod_ciudad,pais,dni);
				 
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					
					

				}
			}
		});	
		
//..................................................fin evento guardar.................................................//	
	
//................................................ evento a clicar sobre label,atras.......................................//
			
			 MouseListener clikLabel2=new MouseListener() {
					   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				setVisible(false);  //CERRAMOS LA VENTANA DE A�ADIR PAIS

				 GestionRestaurante atras=new  GestionRestaurante();
				   atras.setVisible(true);				   }
			};
			 btnAtras.addMouseListener(clikLabel2);
			 
//.................................................fin evento atras.....................................................................//		
			 

}

	

//....................................... metodo para comprobar si una cadena es un numero o letra...........................//

private static boolean isNumeric(String cadena){
	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}

//..................................................fin evento comrobar cadena---------------------------------------//

//.........metodo comprobamos que los valores son cadenas de los campos(nombre,capital,idioma,moneda) y no numeros.........

private static boolean isCadena(String Nombre,String Capital, String Idioma, String Moneda){
	try {
		Integer.parseInt(Nombre);
		Integer.parseInt(Capital);
		Integer.parseInt(Idioma);
		Integer.parseInt(Moneda);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}
}
