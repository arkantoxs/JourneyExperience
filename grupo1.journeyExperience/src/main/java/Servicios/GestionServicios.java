package Servicios;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Restaurantes.EliminarRestaurante_App;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionServicios extends JFrame {

	private JPanel contentPane;
	public  String claves="";
	public  String usuarios="";
	public String nombres="";
	public String nombreTable="";
	public int id_bar=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionServicios frame = new GestionServicios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionServicios() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 332);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(0, 21, 303, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(GestionServicios.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(300, 11, 134, 76);
		contentPane.add(label_1);
		
		JButton button = new JButton("A\u00F1adir");
		
		button.setFont(new Font("Arial", Font.BOLD, 11));
		button.setBounds(91, 142, 101, 44);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Editar");
	
		button_1.setFont(new Font("Arial", Font.BOLD, 11));
		button_1.setBounds(280, 142, 101, 44);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Eliminar");
		
		button_2.setFont(new Font("Arial", Font.BOLD, 11));
		button_2.setBounds(91, 213, 101, 43);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("Mostrar");
		
		button_3.setFont(new Font("Arial", Font.BOLD, 11));
		button_3.setBounds(280, 213, 101, 43);
		contentPane.add(button_3);
		
		JLabel lblNewLabel = new JLabel("Gesti\u00F3n del servicio");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(37, 97, 155, 14);
		contentPane.add(lblNewLabel);
		
		
//...................................llamar a la ventana de A�adir restaurante...................................................
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			AñadirServicios_App pais=new AñadirServicios_App();
			pais.setVisible(true);
			}
		});
// ...............................................fin llamada a laventana de a�adir.........................................//
		
//...................................llamar a la ventana de eliminar pais...................................................
		
		
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EliminarServicios_App eliminarPais=new EliminarServicios_App();
				eliminarPais.setVisible(true);
			}
		});

//...................................fin llamada a la ventana de A�adir pais...................................................
				
//...........................................llamar a la ventana a eliminar pais.....................................................
		
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditarServicios_App editarpais=new EditarServicios_App();
				editarpais.setVisible(true);
			}
		});

//...................................fin llamada a la ventana de eliminar pais...................................................
	
//....................................................llamar a la ventana para mostrar hoteles...........................//
		
		button_3.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		 
			MostrarServiciosApp mostrar=new MostrarServiciosApp(usuarios,claves);
			mostrar.setVisible(true);
		
		}
		});
//...................................fin llamada a la ventana de mostrar pais...................................................

	}

}
