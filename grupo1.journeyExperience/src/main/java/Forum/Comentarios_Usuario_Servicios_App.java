//Assiya Mrabet
package Forum;

import java.awt.BorderLayout;




import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.ListModel;

public class Comentarios_Usuario_Servicios_App extends JFrame {

	private JPanel contentPane;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private int Id_servicio=0;
	private JTextField Usuario1Opinar;
	public String usuarios="";
	public String claves="";

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Comentarios_Usuario_Servicios_App frame = new Comentarios_Usuario_Servicios_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Comentarios_Usuario_Servicios_App(String usuario,String clave) {
		
		this.usuarios=usuario;
		this.claves=clave;
		setTitle("Opini\u00F3n del usuario\r\n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 678);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 27));
		label.setBounds(74, 42, 342, 57);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(Comentarios_Usuario_Servicios_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(463, 11, 151, 88);
		contentPane.add(label_1);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Comentarios_Usuario_Servicios_App.class.getResource("/Vistas/servicios.png")));
		lblNewLabel_1.setBounds(46, 204, 125, 101);
		contentPane.add(lblNewLabel_1);
		
		JLabel Usuario1Comenta = new JLabel("Usuario 1");
		Usuario1Comenta.setFont(new Font("Arial", Font.BOLD, 11));
		Usuario1Comenta.setBounds(58, 385, 67, 14);
		contentPane.add(Usuario1Comenta);
		
		JLabel lblComentarios = new JLabel("Comentarios");
		lblComentarios.setFont(new Font("Arial", Font.BOLD, 11));
		lblComentarios.setBounds(58, 337, 111, 14);
		contentPane.add(lblComentarios);
		
		JButton btnComentar = new JButton("Comentar");
		btnComentar.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnComentar.setBounds(447, 423, 105, 23);
		contentPane.add(btnComentar);
		
		final JLabel lblComentar = new JLabel("Deja tu comentario");
		lblComentar.setFont(new Font("Arial", Font.BOLD, 12));
		lblComentar.setBounds(71, 463, 121, 14);
		contentPane.add(lblComentar);
		lblComentar.setVisible(false);
		
		final JTextPane comentar = new JTextPane();
		comentar.setBounds(69, 488, 523, 91);
		contentPane.add(comentar);
		comentar.setVisible(false);
		
		final JButton btnPublicar = new JButton("Publicar");
		btnPublicar.setFont(new Font("Arial", Font.BOLD, 11));
		btnPublicar.setBounds(463, 590, 89, 23);
		contentPane.add(btnPublicar);
		btnPublicar.setVisible(false);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(602, 188, 17, 451);
		contentPane.add(scrollBar);
		
		JLabel label_3 = new JLabel("P\u00E1gina principal ");
		label_3.setForeground(new Color(0, 0, 0));
		label_3.setFont(new Font("Arial", Font.BOLD, 13));
		label_3.setBounds(45, 143, 124, 23);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("Espacio personal");
		label_4.setForeground(new Color(0, 0, 0));
		label_4.setFont(new Font("Arial", Font.BOLD, 13));
		label_4.setBounds(278, 143, 124, 23);
		contentPane.add(label_4);
		
		JLabel lblCerrarSesion = new JLabel("Cerrar sesi\u00F3n");
		lblCerrarSesion.setForeground(new Color(0, 0, 0));
		lblCerrarSesion.setFont(new Font("Arial", Font.BOLD, 13));
		lblCerrarSesion.setBounds(490, 143, 124, 23);
		contentPane.add(lblCerrarSesion);
		
		Usuario1Opinar = new JTextField();
		Usuario1Opinar.setEditable(false);
		Usuario1Opinar.setBounds(135, 372, 457, 40);
		contentPane.add(Usuario1Opinar);
		Usuario1Opinar.setColumns(10);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(new Color(0, 0, 0));
		btnAtras.setIcon(new ImageIcon(Comentarios_Usuario_Servicios_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(35, 584, 74, 45);
		contentPane.add(btnAtras);
		
		DefaultListModel listaServicios= new DefaultListModel();

		JList listServicio = new JList(listaServicios);
		listServicio.setBounds(194, 223, 385, 117);
		contentPane.add(listServicio);
		
		JLabel lblServicios = new JLabel("Servicios");
		lblServicios.setForeground(Color.BLACK);
		lblServicios.setFont(new Font("Arial", Font.BOLD, 13));
		lblServicios.setBounds(194, 189, 124, 23);
		contentPane.add(lblServicios);
		
	
		
// ........................................COdigo para obtener Restaurantes comentados .........................

		
		  
		 try {
			 String QueryPais="SELECT Servicios.Tipo, Servicios.descripcion, Opinion, Usuario, Servicios.Id_servicio FROM Servicios inner join Opinar_servicios on Servicios.Id_servicio=Opinar_servicios.Id_servicio";
			 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
		     Statement st=Conexion.createStatement();
			 java.sql.ResultSet ResultSet;
			 ResultSet= st.executeQuery(QueryPais);
									 							 
			 //obtener el nombre y opinion del restaurante y guardarlos en array
			 while (ResultSet.next())
			    {
				 	 
				 listaServicios.addElement(ResultSet.getString("Tipo")+":");
				 listaServicios.addElement("     "+ResultSet.getString("descripcion"));
				 listaServicios.addElement("\n");

				 Usuario1Opinar.setText(ResultSet.getString("Opinion"));
				 Usuario1Comenta.setText(ResultSet.getString("Usuario"));
				 Id_servicio=ResultSet.getInt("Id_servicio");

				 }

			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}			
		
// ........................................FIN COdigo para obtener Restaurantes comentados........................
												 
					
		// evento para ir a la pagina principal
		
				 MouseListener PaginaPrincipal=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   Pagina_Principal atras=new  Pagina_Principal();
						   atras.setVisible(true);				   }
					};
					label_3.addMouseListener(PaginaPrincipal);
				
	
				
				// evento para ir al espacio personal
						
						 MouseListener EspacioPeresonal=new MouseListener() {
							   
							  public void mouseReleased(MouseEvent arg0) { }	   
							  public void mousePressed(MouseEvent arg0) {  }		   
							  public void mouseExited(MouseEvent arg0) {  }
							  public void mouseEntered(MouseEvent arg0) { }
							  
							 public void mouseClicked(MouseEvent arg0) {
										  
								   EspacioPersonalApp personal=new  EspacioPersonalApp(usuarios,claves);
								   personal.setVisible(true);				   }
							};
							label_4.addMouseListener(EspacioPeresonal);
				
//..........................................evento para cerrar session......................................................

		MouseListener CambioEstado=new MouseListener() {
								   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
							  
			 public void mouseClicked(MouseEvent arg0) {
											  
			  try {
								           
		       	String estado="no";
		      	String Query = "UPDATE USUARIOS SET Estado='"+estado+"'";
						        	
		        Statement st = Conexion.createStatement();
		        st.executeUpdate(Query);

		        Pagina_Principal principal=new Pagina_Principal();
		        principal.setVisible(true);
		        setVisible(false);
								            
			           // JOptionPane.showMessageDialog(null, "Sesi�n Cerrada");

		     } catch (SQLException ex) {
		          System.out.println(ex.getMessage());
		         // JOptionPane.showMessageDialog(null, "Error a cerrar sesi�n");
		       }  
								 
		}
			};
			lblCerrarSesion.addMouseListener(CambioEstado);
								
	//..........................................FIN evento para cerrar session......................................................

		
//----..................................Mostrar el campo del texto para que el usuario escriba su comentario...............
		btnComentar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comentar.setVisible(true);
				lblComentar.setVisible(true);
				btnPublicar.setVisible(true);
			}
		});
	
	//----..................................fin Mostrar el campo del texto para que el usuario escriba su comentario...............
	
		//publicar el comentario del usuario
		btnPublicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
	//..........................METODO QUE metodo para obtener el usuaario y clave del para guardar su comentarioe en bd.........................................................//

					SQL bd=new SQL();
					
					 	String Estado="si";

						 try {
							 String Query="SELECT Usuario,Clave FROM " + "USUARIOS"+" WHERE Estado= '"+Estado+"'";
							    Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            if (resultSet.next()) {
					            	
					             
							    	try {
										bd.SQLConnection("grupo1","root","");
										bd.insertData_OpinarServicio(Id_servicio,resultSet.getString("Usuario"),resultSet.getString("Clave"), comentar.getText().toString());
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
					            	
					            	
							     
							    }
							    else
							    {
							    JOptionPane.showMessageDialog(null, "No has iniciado la sesi�n");

							      Iniciar_Session_Usuario_App  iniciar=new Iniciar_Session_Usuario_App();
							      iniciar.setVisible(true);
							    }
							   
					         
					        }
						 catch (SQLException ex) {
					            System.out.println(ex.getMessage());
					           // JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
					        }
			 
	  
			}
		});
//..................................................fin de codigo de editar de eliminar pais.............................................//	
	
		//................................ evento a clicar sobre label,atras......................................................
		
		 MouseListener clikLabel=new MouseListener() {
				   
		  public void mouseReleased(MouseEvent arg0) { }	   
		  public void mousePressed(MouseEvent arg0) {  }		   
		  public void mouseExited(MouseEvent arg0) {  }
		  public void mouseEntered(MouseEvent arg0) { }
		  
		 public void mouseClicked(MouseEvent arg0) {
					  
			   setVisible(false);  //CERRAMOS LA VENTANA DE registre

			   Forum_App atras=new  Forum_App(usuarios,claves);
			   atras.setVisible(true);	
			   
		 }
		};
		btnAtras.addMouseListener(clikLabel);
		
//................................ FIN evento a clicar sobre label,atras.....................................................
	}
}
