//Assiya Mrabet
package Forum;
import java.awt.BorderLayout;
import Forum.Comentarios_Usuario_Restaurantes_A;




import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import Bares.MostrarBar_App;
import Forum.Forum_App;
import Hoteles.MostrarHotel_App;
import Main_Comun_App.Pagina_Principal;
import Restaurantes.MostrarRestauranteApp;
import Servicios.MostrarServiciosApp;
import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.ListModel;

public class Forum_App extends JFrame {

	private JPanel contentPane;
	public JLabel ComentarBares; 
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private JLabel VerHotel;
	public String usuarios="";
	public String claves="";

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Forum_App frame = new Forum_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Forum_App(String usuario,String clave) {
		
		this.usuarios=usuario;
		this.claves=clave;
		setTitle("Forum\r\n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 150, 678, 727);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(75, 40, 311, 57);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(Forum_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(449, 30, 149, 91);
		contentPane.add(label_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.inactiveCaption);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(21, 197, 575, 112);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Restaurantes\r\n");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(0, 32, 82, 42);
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		lblNewLabel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		panel.add(lblNewLabel);
		
		JLabel UsuarioResta = new JLabel("");
		UsuarioResta.setFont(new Font("Arial", Font.PLAIN, 12));
		UsuarioResta.setBounds(94, 36, 71, 21);
		UsuarioResta.setHorizontalAlignment(SwingConstants.LEFT);
		UsuarioResta.setAlignmentY(1.0f);
		UsuarioResta.setAlignmentX(1.0f);
		panel.add(UsuarioResta);
		
		JLabel Usuario2Resta = new JLabel("");
		Usuario2Resta.setFont(new Font("Arial", Font.PLAIN, 12));
		Usuario2Resta.setBounds(94, 68, 71, 21);
		Usuario2Resta.setHorizontalAlignment(SwingConstants.LEFT);
		Usuario2Resta.setAlignmentY(1.0f);
		Usuario2Resta.setAlignmentX(1.0f);
		panel.add(Usuario2Resta);
		
		JLabel VerRestaurantes = new JLabel("Ver mas ");
		VerRestaurantes.setForeground(Color.BLUE);
		VerRestaurantes.setFont(new Font("Arial", Font.BOLD, 11));
		VerRestaurantes.setBounds(500, 87, 50, 14);
		panel.add(VerRestaurantes);
		
		JLabel lblNewLabel_3 = new JLabel("Usuario");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_3.setBounds(91, 5, 65, 14);
		panel.add(lblNewLabel_3);
		
		JLabel lblOpinin = new JLabel("Opini\u00F3n\r\n");
		lblOpinin.setFont(new Font("Arial", Font.BOLD, 11));
		lblOpinin.setBounds(184, 5, 71, 14);
		panel.add(lblOpinin);
		
		JLabel lblComentarResta = new JLabel("Comentar");
		lblComentarResta.setForeground(Color.BLUE);
		lblComentarResta.setFont(new Font("Arial", Font.BOLD, 11));
		lblComentarResta.setBounds(10, 68, 64, 14);
		panel.add(lblComentarResta);
		
		DefaultListModel listatresta= new DefaultListModel();
		JList list = new JList(listatresta);
		list.setBounds(92, 26, 456, 63);
		panel.add(list);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.inactiveCaption);
		panel_1.setLayout(null);
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setBounds(21, 318, 575, 112);
		contentPane.add(panel_1);
		
		JLabel lblBares = new JLabel("Bares");
		lblBares.setHorizontalAlignment(SwingConstants.LEFT);
		lblBares.setFont(new Font("Arial", Font.BOLD, 12));
		lblBares.setAlignmentY(1.0f);
		lblBares.setAlignmentX(1.0f);
		lblBares.setBounds(10, 47, 71, 21);
		panel_1.add(lblBares);
		
		JLabel UsuarioBar = new JLabel("");
		UsuarioBar.setHorizontalAlignment(SwingConstants.LEFT);
		UsuarioBar.setFont(new Font("Arial", Font.PLAIN, 12));
		UsuarioBar.setAlignmentY(1.0f);
		UsuarioBar.setAlignmentX(1.0f);
		UsuarioBar.setBounds(91, 47, 71, 21);
		panel_1.add(UsuarioBar);
		
		JLabel Usuario2Bar = new JLabel("");
		Usuario2Bar.setHorizontalAlignment(SwingConstants.LEFT);
		Usuario2Bar.setFont(new Font("Arial", Font.PLAIN, 12));
		Usuario2Bar.setAlignmentY(1.0f);
		Usuario2Bar.setAlignmentX(1.0f);
		Usuario2Bar.setBounds(91, 79, 71, 21);
		panel_1.add(Usuario2Bar);
		
		JLabel VerBar = new JLabel("Ver mas ");
		VerBar.setForeground(Color.BLUE);
		VerBar.setFont(new Font("Arial", Font.BOLD, 11));
		VerBar.setBounds(504, 87, 50, 14);
		panel_1.add(VerBar);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Arial", Font.BOLD, 11));
		lblUsuario.setBounds(86, 7, 65, 14);
		panel_1.add(lblUsuario);
		
		JLabel label_4 = new JLabel("Opini\u00F3n\r\n");
		label_4.setFont(new Font("Arial", Font.BOLD, 11));
		label_4.setBounds(186, 7, 71, 14);
		panel_1.add(label_4);
		
		JLabel lblComentarBares = new JLabel("Comentar");
		lblComentarBares.setForeground(Color.BLUE);
		lblComentarBares.setFont(new Font("Arial", Font.BOLD, 11));
		lblComentarBares.setBounds(10, 79, 64, 14);
		panel_1.add(lblComentarBares);
		
		DefaultListModel listabar= new DefaultListModel();
		JList listBar = new JList( listabar);
		listBar.setBounds(82, 32, 472, 63);
		panel_1.add(listBar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.inactiveCaption);
		panel_2.setLayout(null);
		panel_2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_2.setBounds(21, 441, 575, 112);
		contentPane.add(panel_2);
		
		JLabel lblServicios = new JLabel("Servicios");
		lblServicios.setHorizontalAlignment(SwingConstants.LEFT);
		lblServicios.setFont(new Font("Arial", Font.BOLD, 12));
		lblServicios.setAlignmentY(1.0f);
		lblServicios.setAlignmentX(1.0f);
		lblServicios.setBounds(10, 31, 61, 42);
		panel_2.add(lblServicios);
		
		JLabel UsuarioServicio = new JLabel("");
		UsuarioServicio.setHorizontalAlignment(SwingConstants.LEFT);
		UsuarioServicio.setFont(new Font("Arial", Font.PLAIN, 12));
		UsuarioServicio.setAlignmentY(1.0f);
		UsuarioServicio.setAlignmentX(1.0f);
		UsuarioServicio.setBounds(102, 37, 71, 21);
		panel_2.add(UsuarioServicio);
		
		JLabel Usuario2Servicio = new JLabel("\r\n");
		Usuario2Servicio.setHorizontalAlignment(SwingConstants.LEFT);
		Usuario2Servicio.setFont(new Font("Arial", Font.PLAIN, 12));
		Usuario2Servicio.setAlignmentY(1.0f);
		Usuario2Servicio.setAlignmentX(1.0f);
		Usuario2Servicio.setBounds(102, 69, 71, 21);
		panel_2.add(Usuario2Servicio);
		
		JLabel VerServicio = new JLabel("Ver mas ");
		VerServicio.setForeground(Color.BLUE);
		VerServicio.setFont(new Font("Arial", Font.BOLD, 11));
		VerServicio.setBounds(504, 87, 50, 14);
		panel_2.add(VerServicio);
		
		JLabel lblUsuario_1 = new JLabel("Usuario");
		lblUsuario_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblUsuario_1.setBounds(88, 7, 65, 14);
		panel_2.add(lblUsuario_1);
		
		JLabel label_6 = new JLabel("Opini\u00F3n\r\n");
		label_6.setFont(new Font("Arial", Font.BOLD, 11));
		label_6.setBounds(175, 7, 71, 14);
		panel_2.add(label_6);
		
		JLabel lblComentarServicios = new JLabel("Comentar");
		lblComentarServicios.setForeground(Color.BLUE);
		lblComentarServicios.setFont(new Font("Arial", Font.BOLD, 11));
		lblComentarServicios.setBounds(7, 73, 64, 14);
		panel_2.add(lblComentarServicios);
		
		DefaultListModel listaServicio= new DefaultListModel();

		JList listServicios = new JList(listaServicio);
		listServicios.setBounds(84, 27, 470, 63);
		panel_2.add(listServicios);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(624, 198, 17, 470);
		contentPane.add(scrollBar);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(SystemColor.inactiveCaption);
		panel_4.setLayout(null);
		panel_4.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_4.setBounds(21, 564, 575, 113);
		contentPane.add(panel_4);
		
		JLabel lblHoteles = new JLabel("Hoteles");
		lblHoteles.setHorizontalAlignment(SwingConstants.LEFT);
		lblHoteles.setFont(new Font("Arial", Font.BOLD, 12));
		lblHoteles.setAlignmentY(1.0f);
		lblHoteles.setAlignmentX(1.0f);
		lblHoteles.setBounds(10, 31, 61, 42);
		panel_4.add(lblHoteles);
		
		JLabel UsuarioHotel = new JLabel("\r\n");
		UsuarioHotel.setHorizontalAlignment(SwingConstants.LEFT);
		UsuarioHotel.setFont(new Font("Arial", Font.PLAIN, 12));
		UsuarioHotel.setAlignmentY(1.0f);
		UsuarioHotel.setAlignmentX(1.0f);
		UsuarioHotel.setBounds(102, 42, 71, 21);
		panel_4.add(UsuarioHotel);
		
		JLabel Usuario2Hotel = new JLabel("");
		Usuario2Hotel.setHorizontalAlignment(SwingConstants.LEFT);
		Usuario2Hotel.setFont(new Font("Arial", Font.PLAIN, 12));
		Usuario2Hotel.setAlignmentY(1.0f);
		Usuario2Hotel.setAlignmentX(1.0f);
		Usuario2Hotel.setBounds(102, 64, 71, 21);
		panel_4.add(Usuario2Hotel);
		
		JLabel Opinar2Hotel = new JLabel("");
		Opinar2Hotel.setHorizontalAlignment(SwingConstants.LEFT);
		Opinar2Hotel.setFont(new Font("Arial", Font.PLAIN, 11));
		Opinar2Hotel.setAlignmentY(1.0f);
		Opinar2Hotel.setAlignmentX(1.0f);
		Opinar2Hotel.setBounds(185, 64, 364, 21);
		panel_4.add(Opinar2Hotel);
		
		VerHotel = new JLabel("Ver mas ");
		VerHotel.setForeground(Color.BLUE);
		VerHotel.setFont(new Font("Arial", Font.BOLD, 11));
		VerHotel.setBounds(502, 88, 50, 14);
		panel_4.add(VerHotel);
		
		JLabel lblUsuario_2 = new JLabel("Usuario");
		lblUsuario_2.setFont(new Font("Arial", Font.BOLD, 11));
		lblUsuario_2.setBounds(89, 6, 65, 14);
		panel_4.add(lblUsuario_2);
		
		JLabel label_9 = new JLabel("Opini\u00F3n\r\n");
		label_9.setFont(new Font("Arial", Font.BOLD, 11));
		label_9.setBounds(171, 6, 71, 14);
		panel_4.add(label_9);
		
		JLabel lblComentarHoteles = new JLabel("Comentar");
		lblComentarHoteles.setForeground(Color.BLUE);
		lblComentarHoteles.setFont(new Font("Arial", Font.BOLD, 11));
		lblComentarHoteles.setBounds(10, 64, 64, 14);
		panel_4.add(lblComentarHoteles);
		
		DefaultListModel listaHoteles= new DefaultListModel();

		JList listHotel = new JList( listaHoteles);
		listHotel.setBounds(93, 31, 456, 63);
		panel_4.add(listHotel);
		
		JLabel label_3 = new JLabel("P\u00E1gina principal ");
		label_3.setForeground(new Color(0, 0, 0));
		label_3.setFont(new Font("Arial", Font.BOLD, 13));
		label_3.setBounds(57, 151, 124, 23);
		contentPane.add(label_3);
		
		JLabel lblEspacioPersonal = new JLabel("Espacio personal");
		lblEspacioPersonal.setForeground(new Color(0, 0, 0));
		lblEspacioPersonal.setFont(new Font("Arial", Font.BOLD, 13));
		lblEspacioPersonal.setBounds(264, 151, 124, 23);
		contentPane.add(lblEspacioPersonal);
		
		JLabel lblCerrarSessin = new JLabel("Cerrar sessi\u00F3n");
		lblCerrarSessin.setForeground(new Color(0, 0, 0));
		lblCerrarSessin.setFont(new Font("Arial", Font.BOLD, 13));
		lblCerrarSessin.setBounds(479, 151, 124, 23);
		contentPane.add(lblCerrarSessin);
		
		
	// ........................................COdigo para obtener OPINIONES del usuario sobre restaurantes .........................
		

		  
		 try {
			 String QueryPais="SELECT * FROM " + "Opinar_restuarante";
			 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
		     Statement st=Conexion.createStatement();
			 java.sql.ResultSet ResultSet;
			 ResultSet= st.executeQuery(QueryPais);
									 							 
			 //obtener el nombre y opinion del restaurante y guardarlos en array
			 while (ResultSet.next())
			    {
				 	 
				 listatresta.addElement(ResultSet.getString("Usuario")+":");
				 listatresta.addElement("     "+ResultSet.getString("Opinion"));
				 //listatresta.addElement("\n");


			    } 
			    } catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}	
	
// ........................................FIN COdigo para obtener opiniones del usuario sobre el restaurantes.........................
										 
			
// ........................................COdigo para obtener OPINIONES del usuario sobre el hotel hotel .........................

			 try {
				 String Queryhotel="SELECT * FROM " + "Opinar_hotel";
				 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
			     Statement sto=Conexion.createStatement();
				 java.sql.ResultSet ResultSeto;
				 ResultSeto= sto.executeQuery(Queryhotel);
										 							 
				 // rellenamos el array con los datos del hotel buscados
				 while (ResultSeto.next())
				    {
					 	 
					 listaHoteles.addElement(ResultSeto.getString("Usuario")+":");
					 listaHoteles.addElement("     "+ResultSeto.getString("Opinion"));
					// listaHoteles.addElement("\n");
					 }
			 } catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}	
			
// ........................................FIN COdigo para obtener opiniones del usuario sobre el hotal.........................

			 
				
// ........................................COdigo para obtener OPINIONES del usuario sobre el bare .........................
						
		
						  
						 try {
							 String Querybar="SELECT * FROM " + "Opinar_bar";
							 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
						     Statement st1=Conexion.createStatement();
							 java.sql.ResultSet ResultSet1;
							 ResultSet1= st1.executeQuery(Querybar);
													 							 
							 // rellenamos el array con los datos del hotel buscados
							 while (ResultSet1.next())
							    {
								 	 
								 listabar.addElement(ResultSet1.getString("Usuario")+":");
								 listabar.addElement("     "+ResultSet1.getString("Opinion"));
								// listabar.addElement("\n");
								 }
							// recoremos el array mostramos en la pantalla los resultados	  
				
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}	
// ........................................FIN COdigo para obtener opiniones del usuario sobre el bares........................			 
		
						 
// ........................................COdigo para obtener OPINIONES del usuario sobre los servicios .........................
							
	
						  
						 try {
							 String QuerySer="SELECT * FROM " + "Opinar_servicios";
							 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
						     Statement st2=Conexion.createStatement();
							 java.sql.ResultSet ResultSet2;
							 ResultSet2= st2.executeQuery(QuerySer);
													 							 
							 // rellenamos el array con los datos del servicio buscados con opiniones
							 while (ResultSet2.next())
							    {
								 	 
								 listaServicio.addElement(ResultSet2.getString("Usuario")+":");
								 listaServicio.addElement("     "+ResultSet2.getString("Opinion"));
								// listaServicio.addElement("\n");
								 }
							// recoremos el array mostramos en la pantalla los resultados	  
							
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}	
	// ........................................FIN COdigo para obtener opiniones del usuario sobre servicios........................			 
	
	
	//------------------ir a la lisa de restaurantes k hay en la bd.............................	
						 
						 MouseListener PaginaPrincipal=new MouseListener() {
							   
							  public void mouseReleased(MouseEvent arg0) { }	   
							  public void mousePressed(MouseEvent arg0) {  }		   
							  public void mouseExited(MouseEvent arg0) {  }
							  public void mouseEntered(MouseEvent arg0) { }
							  
							 public void mouseClicked(MouseEvent arg0) {
										  
								   MostrarRestauranteApp atras=new  MostrarRestauranteApp(usuarios,claves);
								   atras.setVisible(true);				   }
							};
							lblComentarResta.addMouseListener(PaginaPrincipal);
//------------------ FIN ir a la lisa de hoteles k hay en la bd.............................		
							
	//------------------ir a la lisa de restaurantes k hay en la bd.............................	
							 
							 MouseListener hoteles=new MouseListener() {
								   
								  public void mouseReleased(MouseEvent arg0) { }	   
								  public void mousePressed(MouseEvent arg0) {  }		   
								  public void mouseExited(MouseEvent arg0) {  }
								  public void mouseEntered(MouseEvent arg0) { }
								  
								 public void mouseClicked(MouseEvent arg0) {
											  
									   MostrarHotel_App atras=new  MostrarHotel_App(usuarios,claves);
									   atras.setVisible(true);				   }
								};
								lblComentarHoteles.addMouseListener(hoteles);
	//------------------ FIN ir a la lisa de hotel k hay en la bd.............................			

	//------------------ir a la lisa de restaurantes k hay en la bd.............................	
								 
								 MouseListener servicios=new MouseListener() {
									   
									  public void mouseReleased(MouseEvent arg0) { }	   
									  public void mousePressed(MouseEvent arg0) {  }		   
									  public void mouseExited(MouseEvent arg0) {  }
									  public void mouseEntered(MouseEvent arg0) { }
									  
									 public void mouseClicked(MouseEvent arg0) {
												  
										   MostrarServiciosApp atras=new  MostrarServiciosApp(usuarios,claves);
										   atras.setVisible(true);				   }
									};
									lblComentarServicios.addMouseListener(servicios);
		//------------------ FIN ir a la lisa de servicios k hay en la bd.............................		
									
	//------------------ir a la lisa de bar k hay en la bd.............................	
									 
									 MouseListener bar=new MouseListener() {
										   
										  public void mouseReleased(MouseEvent arg0) { }	   
										  public void mousePressed(MouseEvent arg0) {  }		   
										  public void mouseExited(MouseEvent arg0) {  }
										  public void mouseEntered(MouseEvent arg0) { }
										  
										 public void mouseClicked(MouseEvent arg0) {
													  
											   MostrarBar_App atras=new  MostrarBar_App(usuarios,claves);
											   atras.setVisible(true);				   }
										};
										lblComentarServicios.addMouseListener(servicios);
			//------------------ FIN ir a la lisa de hotel k hay en la bd.............................		
							
// ..........................evento para ir a la pagina principal..................................................
		
		 MouseListener Principal=new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   Pagina_Principal atras=new  Pagina_Principal();
				   atras.setVisible(true);				   }
			};
			label_3.addMouseListener(Principal);
	
			
	
		// evento para ir al espacio personal
				
				 MouseListener EspacioPeresonal=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   EspacioPersonalApp personal=new  EspacioPersonalApp(usuarios,claves);
						   personal.setVisible(true);				   }
					};
					lblEspacioPersonal.addMouseListener(EspacioPeresonal);
		
//.................................................. evento para cerrar session.................................................
		
		
		// evento para ir al formulario comentar restaurantes
	//------------------------------------//evento para ver todos los comentarios del restaurante x.................................................................
			 
					 MouseListener ComentarRestaurantes=new MouseListener() {
							   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   Comentarios_Usuario_Restaurantes_A ComResta=new  Comentarios_Usuario_Restaurantes_A(usuarios,claves);
						   ComResta.setVisible(true);		
						   setVisible(false);
						   }
					};
			   VerRestaurantes.addMouseListener(ComentarRestaurantes);
					    						   
//------------------------------------fin Codigo.................................................................
					 
					
	//------------------------------------//evento para ver todos los comentarios del Hotel x.................................................................
				 
				 MouseListener ComentarHotel=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
					   Comentarios_Usuario_Hoteles_App ComVerHotel=new  Comentarios_Usuario_Hoteles_App(usuarios,claves);
					   ComVerHotel.setVisible(true);		
					   setVisible(false);
					   }
				};
		 VerHotel.addMouseListener(ComentarHotel);
				    						   
//------------------------------------fin Codigo.................................................................	
		
		
		
		
//.....................................evento para ver todos los comentarios del bar x-----------------------................
		 
		 MouseListener ComentarBaros=new MouseListener() {
				   
		  public void mouseReleased(MouseEvent arg0) { }	   
		  public void mousePressed(MouseEvent arg0) {  }		   
		  public void mouseExited(MouseEvent arg0) {  }
		  public void mouseEntered(MouseEvent arg0) { }
		  
		 public void mouseClicked(MouseEvent arg0) {
					  
			   Comentarios_Usuario_Bares_App ComResta=new  Comentarios_Usuario_Bares_App(usuarios,claves);
			   ComResta.setVisible(true);		
			   setVisible(false);
			   }
		};
   VerBar.addMouseListener(ComentarBaros);
		    						   
//------------------------------------fin Codigo.................................................................
		
   
 //.....................................//evento para ver todos los comentarios del servicio x-----------------------................
	 
	 MouseListener ComentarServicio=new MouseListener() {
			   
	  public void mouseReleased(MouseEvent arg0) { }	   
	  public void mousePressed(MouseEvent arg0) {  }		   
	  public void mouseExited(MouseEvent arg0) {  }
	  public void mouseEntered(MouseEvent arg0) { }
	  
	 public void mouseClicked(MouseEvent arg0) {
				  
		   Comentarios_Usuario_Servicios_App ComResta=new  Comentarios_Usuario_Servicios_App(usuarios,claves);
		   ComResta.setVisible(true);		
		   setVisible(false);
		   }
	};
VerServicio.addMouseListener(ComentarServicio);
	    						   
//------------------------------------fin Codigo.................................................................
		
		//evento para ver todos los comentarios del entretinimiento x
		
		

//..........................................evento para cerrar session......................................................

			   MouseListener CambioEstado=new MouseListener() {
						   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							  try {
						           
						        	String estado="no";
						        	String Query = "UPDATE USUARIOS SET Estado='"+estado+"'";
						        	
						            Statement st = Conexion.createStatement();
						            st.executeUpdate(Query);

						             Pagina_Principal principal=new Pagina_Principal();
						             principal.setVisible(true);
						             setVisible(false);
						            
						           // JOptionPane.showMessageDialog(null, "Se ha modificado los datos");

						        } catch (SQLException ex) {
						            System.out.println(ex.getMessage());
						           // JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
						        }  
							 
							 }
						};
						lblCerrarSessin.addMouseListener(CambioEstado);
						
		//..........................................FIN evento para cerrar session......................................................	


		
		
		
		
	}
}
