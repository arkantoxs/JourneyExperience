/**
 * 
 */
package WipAssiya;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;

/**
 * @author Assiya
 *
 */
public class SQL {
	
	private static Connection Conexion = null;


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            //JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	            System.out.println("Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            //JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	            System.out.println("NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            //JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	            System.out.println("NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            //JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		            System.out.println("Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            //JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		            System.out.println("NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
	
// METODOS RELACIONADOS CON LA TABLA DEL USUARIO
	
 //...........................................METODO QUE INSERTA USUARIOS EN NUESTRA BASE DE DATOS..........................//
		
		  public void insertDataUsuarios(String USUARIO,String CLAVE, String NOMBRE, String APELLIDO, String EMAIL, int CODIGO_POSTAL, String ULTIMO_VIAJE, String PAIS_DESTINO, String OTROS_DATOS) {
			  try {
				  	String Estado="si";
				  	//System.out.println(USUARIO+" "+CLAVE+" "+" "+NOMBRE+" "+APELLIDO+" "+EMAIL+" "+CODIGO_POSTAL+" "+ULTIMO_VIAJE+" "+PAIS_DESTINO+" "+OTROS_DATOS+" "+Estado);
			        String Query = "INSERT INTO " + "USUARIOS" + " VALUES("
			            	+ "'"+ USUARIO + "',"
			                + "'"+ CLAVE + "',"
			            	+ "'"+ NOMBRE + "',"
			            	+ "'"+ APELLIDO + "',"
			            	+ "'"+ EMAIL + "',"
			            	 + CODIGO_POSTAL + ","
			            	+ "'"+ ULTIMO_VIAJE + "',"
			            	+ "'"+ PAIS_DESTINO + "',"
			            	+ "'"+ Estado + "',"
			                + "'"+ OTROS_DATOS +  "')";
			        
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
			  
		   }	

  //...........................................FIN METODO QUE INSERTA USUARIOS EN NUESTRA BASE DE DATOS..........................//
		  
	//..............................METODO OBTENER VALORES DE LA TABLA USUARIO  para iniciar sesion .............................//		 
			
		  public boolean getValuesUsuario_Sesion(String usuario, String clave) {
				 
			  	boolean estado=false;
		        try {
			           String Query = "SELECT * FROM " + "USUARIOS" + " WHERE Usuario = '" + usuario+ "'"+" and Clave = '" + clave+ "'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				    if (resultSet.next()) {
				    	estado=true;
				    	UpdatedeleteRecord_EStadoUsuario(usuario,clave); //lllamo al metodo para que me cambia de estado del usuario cuando se inica sesion
				     
				    }
				    else
				    {
				      estado=false;	
				    }
				   

		        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        return estado;
			    }
	//..............................FIN METODO OBTENER DATOS PARA INICIAR SESION................................................
	
//..........................METODO QUE edita esatado del usuario en BD .........................................................//
		  
			 public void UpdatedeleteRecord_EStadoUsuario(String usuario,String clave) {
			        try {
			           
			        	String estado="si";
			        	String Query = "UPDATE USUARIOS SET Estado='"+estado+"'"+" WHERE Clave='"+clave+"'"+"AND Usuario='"+usuario+"'";
			        	
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			          
			            
			           // JOptionPane.showMessageDialog(null, "Se ha modificado los datos");

			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			           // JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
			        }
			 }	  
	
			 
//..................................................fin de codigo de editar de eliminar pais.............................................//	

//..........................METODO QUE edita esatado del usuario en BD .........................................................//
			  
 public void UpdatedeleteRecord_EStado(String usuario,String clave) 
 {
		
	 	String Estado="si";

		 try {
			 String Query="SELECT * FROM " + "USUARIOS"+" WHERE Estado= '"+Estado+"'";
			    Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            if (resultSet.next()) {
			    	
	            	//EspacioPersonalApp  personal=new EspacioPersonalApp();
	            	
	            	//personal.setVisible(true);
	            	
			     
			    }
			    else
			    {
			    	
			     // Iniciar_Session_Usuario_App  iniciar=new Iniciar_Session_Usuario_App();
			      //iniciar.setVisible(true);
			    }
			   
	         
	        }
		 catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	           // JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
	        }
	
 }				 
//..................................................fin de codigo de editar de eliminar pais.............................................//	

		  

  //.............................. METODO  cambair contrase�a...............................................
	  

		 public void CambiarContrasenya(String contraNueva,String contraActual)
		 {

			 try {
		           
                    System.out.println(contraNueva+contraActual);
		        	String Query = "UPDATE USUARIOS SET Clave ='"+contraNueva+"'"+" WHERE Clave='"+contraActual+"'";
		        			        	
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		          
		            
		            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
		        }
		 }	 
			 
		  
	//.............................. FIN METODO cambair contrase�a...............................................

		  
//..............................METODO OBTENER VALORES DE LA TABLA USUARIO  para iniciar sesion .............................//		 
			
	 public boolean getValuesUsuario_Clave(String clave) {
				 
			  	boolean estado=false;
		        try {
			           String Query = "SELECT * FROM " + "USUARIOS" + " WHERE Clave = '" + clave+ "'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				    if (resultSet.next()) {
				    	estado=true;
				     
				    }
				    else
				    {
				      estado=false;	
				    }
				   

		        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        return estado;
			    }
	//..............................FIN METODO OBTENER DATOS PARA INICIAR SESION................................................
	  
//..............................METODO OBTENER VALORES DE LA TABLA USUARIO......................................
		  public String[] getValuesUsuario (String valor)
		  {

		        String cadena[] = new String[10];
		        
		        try {
		            String Query = "SELECT * FROM USUARIOS WHERE USUARIO='"+valor+"'";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		       
		            while (resultSet.next()) {
		            	
		            	cadena[0]=resultSet.getString("USUARIO");
		            	cadena[1]=resultSet.getString("CLAVE");
		            	cadena[2]=resultSet.getString("NOMBRE");
		            	cadena[3]=resultSet.getString("APELLIDO");
		            	cadena[4]=resultSet.getString("EMAIL");
		            	cadena[5]=resultSet.getString("CODIGO_POSTAL");
		            	cadena[6]=resultSet.getString("ULTIMO_VIAJE");
		            	cadena[7]=resultSet.getString("PAIS_DESTINO");
		            	cadena[8]=resultSet.getString("OTROS_DATOS");
		            	cadena[9]=resultSet.getString("FOTO");
		                
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return cadena;
		    }		 	
		  
//..............................FIN METODO OBTENER VALORES DE LA TABLA USUARIO......................................

//..........................METODO QUE edita valores deL USUARIO .........................................................//
		  
public void UpdatedeleteRecord_Usuario(String usuario,String nombre,String Apellido,String Email,int Codigo,String ultimoViaje,String paisDestino, String Otros) {
			        try {
			           
			        	//System.out.println(nombre+" "+Apellido+" "+Email+" "+Codigo+" "+ultimoViaje+" "+paisDestino+" "+Otros);
			        	String Query = "UPDATE USUARIOS SET NOMBRE ='"+nombre+"',"+"APELLIDO='"+Apellido+"',"+"EMAIL='"+Email+"',"
			        			        +"CODIGO_POSTAL="+Codigo+","+"ULTIMO_VIAJE='"+ultimoViaje+"',"+"PAIS_DESTINO='"+paisDestino+"',"+"OTROS_DATOS='"+Otros+"'"
		        			       +" WHERE USUARIO='"+usuario+"'";
			        	
			        	
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			          
			            
			            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");
			            System.out.println(nombre);


			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
			        }
			 }	  
	
			 
//..................................................fin metodo de eliminar pais.............................................//	
			 
//// METODOS RELACIONADOS CON LA TABLA DE PAISES

 //...........................................METODO QUE INSERTA PAISES EN NUESTRA BASE DE DATOS..........................//
		  
		  public void insertDataPaises(String NOMBRE,String MONEDA, String IDIOMA, String BANDERA, String CAPITAL, int HABITANTES, String CONTINENTE, String Dni_Admin) {
		     
			  System.out.println(NOMBRE+MONEDA+IDIOMA+BANDERA+CAPITAL+HABITANTES+CONTINENTE+Dni_Admin);
 
			  try {
			        String Query = "INSERT INTO " + "PAISES" + " VALUES("
			            	+ "'"+ NOMBRE + "',"
			                + "'"+ MONEDA + "',"
			            	+ "'"+ IDIOMA + "',"
			            	+ "'"+ BANDERA + "',"
			            	+ "'"+ CAPITAL + "',"
			            	+ "'"+ HABITANTES + "',"
			            	+ "'"+ CONTINENTE + "',"
			            	+ "'"+ Dni_Admin + "')";
			        
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       System.out.println(NOMBRE+MONEDA+IDIOMA+BANDERA+CAPITAL+HABITANTES+CONTINENTE+Dni_Admin);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
		  
//..........................METODO QUE ELIMINA el pais seleccionado .........................................................//
		  
			 public void deleteRecord_Pais( String nombre) {
			        try {
			            String Query = "DELETE FROM " + "PAISES" + " WHERE NOMBRE = '" + nombre+ "'";
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "el pais "+nombre+" se ha elimonado de basa de datos");
			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
			        }
			 }	  
	
//..................................................fin metodo de eliminar pais.............................................//

			 
//...........................................metodo para obtener todos los paises que hay la BD..............................//
			  
			  public String[] getValuesPais ()
			  {
			        String num[]=new String[20];
			        int cont=0; 
			        int tamany;
			        try {
			            String Query = "SELECT * FROM " + "PAISES";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			       
			            while (resultSet.next()) {
			            	
			    	        num[cont]=resultSet.getString("NOMBRE");
			            	cont++;
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			        
			        return num;
			    }
			
//...........................................FIN metodo para obtener todos los paises que hay la BD..............................//

			  
//..........................METODO QUE edita valores de la tabla del pais seleccionado .........................................................//
			  
			 public void UpdatedeleteRecord_Pais(String nombre,String capital,String continente,int habitant,String idioma,String moneda,String Bandera,String dni) {
			        try {
			           
			        	String Query = "UPDATE PAISES SET MONEDA ='"+moneda+"',"+"IDIOMA='"+idioma+"',"+"BANDERA='"+Bandera+"',"
			        	               +"CAPITAL='"+capital+"',"+"HABITANTES="+habitant+","+"NOMBRE_CONT='"+continente+"',"+"DNI_Admin='"+dni+"'"
			        			       +" WHERE NOMBRE='"+nombre+"'";
			        	
			        	
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			          
			            
			            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
			        }
			 }	  
	
			 
//..................................................fin metodo de editar pais.............................................//
			 
	
			 
			 
//METEDOD RELACIONADOS CON LA TABLA DE CIUDADES DE BASE DE DATOS
			 
			 
//...........................................METODO QUE INSERTA CIUDADES EN NUESTRA BASE DE DATOS..........................//

		  public void insertDataCiudadesPueblos( String NOMBRE, String PAIS,String DESCRIPCION,String Dni_Admin) {
			  try {
			        String Query = "INSERT INTO " + "CIUDADES_PUEBLOS" + " VALUES("
			            	+ "SEQ_Ciudades.NEXTVAL"+","
			                + "'"+ NOMBRE + "',"
			                + "'"+ DESCRIPCION + "',"
			            	+ "'"+ PAIS + "',"	
			         	    + "'"+ Dni_Admin + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			       
		   } catch (SQLException ex) {
		       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		   }
	   } 
//...........................................................fin metodo de insertar ciudades a BD...........................//		  

		  
//...........................................metodo para obtener todos los ciudade que hay la BD..............................//
		  
		  public String[] getValuesCiudad()
		  {
		        String num[]=new String[20];
		        int cont=0; 
		        int tamany;
		        try {
		            String Query = "SELECT * FROM " + "CIUDADES_PUEBLOS";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		       
		            while (resultSet.next()) {
		            	
		    	        num[cont]=resultSet.getString("NOMBRE");
		            	cont++;
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return num;
		    }
		
//...........................................FIN metodo para obtener todas las ciudades que hay la BD..............................//

//...........................................metodo para obtener EL ID DE LA ciudadad  buscada..............................//
		  
		  public String getValues_NombreCiudad(int cod)
		  {
		        String nom="";
		   
		        try {
		            String Query = "SELECT * FROM " + "CIUDADES_PUEBLOS"+ " WHERE id_ciudad="+ cod;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		       
		            while (resultSet.next()) {
		            	
		    	        nom=resultSet.getString("NOMBRE");
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return nom;
		    }
		
//...........................................FIN metodo para obtener todas las ciudades que hay la BD..............................//


		  
//..........................METODO QUE edita valores de la tabla de la ciudad seleccionada .........................................................//
		  
			 public void UpdatedeleteRecord_Ciudad(String nombre,String Admin,String Pais,String Descripcion,int cod_ciudad) {
			        try {
			           
			        	
			        	String Query = "UPDATE CIUDADES_PUEBLOS SET NOMBRE='"+nombre+"',"+"DESCRIPCION='"+Descripcion+"',"
			        	               +"NOMBRE_PAIS='"+Pais+"',"+"DNI_ADMIN='"+Admin+"'"
			        			       +" WHERE ID_CIUDAD="+cod_ciudad;
			        	
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			          
			            
			            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
			        }
			 }	  
	
			 
//..................................................fin metodo de eliminar pais.............................................//	
			 
//...................................................metdoo para obtener el codigo de la ciudad selecionada...............................			
			  
			  
			  public int ObtenerCod_Ciudad(String nombre)
			  {
			       int id_Ciudad=0;
			       

			        try {
			            String Query = "SELECT * FROM " + "CIUDADES_PUEBLOS" +" WHERE NOMBRE='"+nombre+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            
			            //tamany=resultSet.getRow();
			            while (resultSet.next()) {
			            	
			    	        id_Ciudad=resultSet.getInt("ID_CIUDAD");

			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			        
			        return id_Ciudad;
			    }
			  
			  
//............................................FIN metodo de obtener el dni de el admin....................................	
			 
//..........................METODO QUE ELIMINA la ciudad seleccionada .........................................................//
			  
				 public void deleteRecord_Ciudad( int cod, String nombre) {
				        try {
				            String Query = "DELETE FROM " + "CIUDADES_PUEBLOS" + " WHERE ID_CIUDAD = '" + cod+ "'";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            JOptionPane.showMessageDialog(null, "Ciudad "+nombre+" se ha elimonado de basa de datos");
				        } catch (SQLException ex) {
				            System.out.println(ex.getMessage());
				            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
				        }
				 }	  
		
	//..................................................fin metodo de eliminar ciudad.............................................//			 
			 
			 
			 


		  
		  
// METODOS RELACIONADOS CON EL HOTEL 
		  
//.............................................codigo para inserata hotel-....................................
		  
 public void insertHotel(String NOMBRE, int CONTACTO, String SERVICIOS, String TARIFAS, String DESCRIPCION) {
			    try {
			        String Query = "INSERT INTO " + "HOTELES" + " VALUES("
			        		+ "SEQ_Hoteles.NEXTVAL"+","
			                + "'"+ NOMBRE + "',"
			                +  CONTACTO + ","
			                + "'"+ SERVICIOS + "',"
			                + "'"+ TARIFAS + "',"
			            	+ "'"+ DESCRIPCION + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
//.............................................FIN codigo para inserata hotel-....................................

//.............................................codiggo para inserata en la tabla de tener hotel-....................................
		  
 public void insert_TenerHotel_Bar_Servicio_Resta_Entre(String table_name,int cod, int cod_ciudad, String nombre_pais, String dni_Admin) {
	    try {
	    	
	    	System.out.println(table_name+" "+cod+" "+cod_ciudad+" "+nombre_pais+" "+dni_Admin);
	        String Query = "INSERT INTO " + table_name + " VALUES("
	        		+ cod+","
	                +  cod_ciudad + ","
	                + "'"+ nombre_pais + "',"
	            	+ "'"+ dni_Admin + "')";
	       Statement st = Conexion.createStatement();
	       st.executeUpdate(Query);
	       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	   } catch (SQLException ex) {
	       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	   }
}

//.............................................FIN Evento para insertar en tener hotel-....................................
		  
//.............................................codigo para buscar  id del hotel-....................................
 
 public int Obtener_IdHotel(String NOMBRE, int CONTACTO, String SERVICIOS, String TARIFAS, String DESCRIPCION) {
	    
	 int codigo=0;
	 
	 try {
         String Query = "SELECT Id_hotel FROM " + "HOTELES" + " WHERE NOMBRE = '" + NOMBRE+ "'"+" and CONTACTO = " + CONTACTO+ "" 
        		 			+" and SERVICIOS = '" + SERVICIOS+ "'"+" and TARIFAS = '" + TARIFAS+ "'"+" and DESCRIPCION = '" + DESCRIPCION+ "'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	
	            while (resultSet.next()) {
	            	
	            	
	            	codigo=resultSet.getInt("Id_hotel");
	                
	            }

  } catch (SQLException ex) {
          JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
  }
	 return codigo;
	 
}

//.............................................FIN codigo para obtenrer id hotel-....................................
		  		  	
//...........................................metodo para obtener todos los hoteles que hay la BD..............................//
 
 public String[] getValuesHotel()
 {
       String num[]=new String[20];
       int cont=0; 
       int tamany;
       try {
           String Query = "SELECT * FROM " + "HOTELES";
           Statement st = Conexion.createStatement();
           java.sql.ResultSet resultSet;
           resultSet = st.executeQuery(Query);
      
           while (resultSet.next()) {
           	
   	        num[cont]=resultSet.getString("NOMBRE");
           	cont++;
           }

       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
       }
       
       return num;
   }

//...........................................FIN metodo para obtener todas los hoteles que hay la BD..............................//		 
			 
			 
//..........................METODO QUE edita valores de la tabla del hotel seleccionado .........................................................//
 
 public void UpdatedeleteRecord_hotel(int id_hotel,String nombre,int contacto,String servicios,String tarifas,String descripcion) {
        try {
           
        	String Query = "UPDATE HOTELES SET Nombre ='"+nombre+"',"+"Contacto="+contacto+","+"Servicios='"+
        	                servicios+"',"+"Tarifas='"+tarifas+"',"+"Descripcion='"+descripcion+"'"
        			       +" WHERE Id_hotel="+id_hotel;
        	
        	
            Statement st = Conexion.createStatement();
            st.executeUpdate(Query);

          
            
            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
        }
 }	  

 
//..................................................fin metodo de editar hotel.............................................//
 
//..........................METODO QUE edita datos de la tabla de TENER hotel seleccionado .........................................................//
 
public void UpdatedeleteRecord_Tenerhotel(int id_hotel,int id_ciudad,String nombrePais,String DniAdmin) {
       try {
          
       	String Query = "UPDATE TENER_HOTEL SET id_ciudad="+id_ciudad+","+"Nombre_pais='"+nombrePais+"',"+"Dni_admin='"+
       	                DniAdmin+"'"+" WHERE Id_hotel="+id_hotel;
       	
           				Statement st = Conexion.createStatement();
           				st.executeUpdate(Query);

         
           
           JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


       } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
       }
}	  


//..................................................fin metodo de editar hotel.............................................//

//.............................METODO QUE eliminar HOTEL DE NUESTRA BASE DE DATOS......................................

public void deleteRecord_EliminarHotel(int ID) {
       try {
           String Query = "DELETE FROM " + "HOTELES" + " WHERE Id_Hotel =" + ID;
           Statement st = Conexion.createStatement();
           st.executeUpdate(Query);

       } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
       }
   }
 
//............................. FIN METODO QUE eliminar HOTEL de BASE DE DATOS......................................

//.............................METODO QUE eliminar HOTEL DE la tabla tener _hote BASE DE DATOS......................................

public void deleteRecord_Eliminar_TenerHotel(int ID) {
     try {
         String Query = "DELETE FROM " + "tener_hotel" + " WHERE Id_Hotel =" + ID;
         Statement st = Conexion.createStatement();
         st.executeUpdate(Query);

     } catch (SQLException ex) {
         System.out.println(ex.getMessage());
         JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
     }
 }

//............................. FIN METODO QUE eliminar HOTEL de la tabla tener_hotel de BASE DE DATOS......................................


									// METODOS RELACIONADOS CON LA TABLA DE RESTAURANTES

//.............................................codigo para inserata restaurante-....................................

public void insertarRestaurante(String NOMBRE, int CONTACTO, String ESPECIALIDAD, String TARIFAS, String HORARIO, String DESCRIPCION) {
			   
	   try {
			        String Query = "INSERT INTO " + "Restaurantes" + " VALUES("
			        		+ "SEQ_restaurantes.NEXTVAL"+","
			                + "'"+ NOMBRE + "',"
			                +  CONTACTO + ","
			                + "'"+ ESPECIALIDAD + "',"
			                + "'"+ TARIFAS + "',"
			                + "'"+ HORARIO + "',"
			            	+ "'"+ DESCRIPCION + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
//.............................................FIN codigo para inserata hotel-....................................

//.............................................codigo para buscar  id del restaurante-....................................

public int Obtener_IdRestaurante(String NOMBRE, int CONTACTO, String especialidad, String TARIFAS, String DESCRIPCION) {
	    
	 int codigo=0;
	 
	 try {
       String Query = "SELECT Id_restaurante FROM " + "restaurantes" + " WHERE NOMBRE = '" + NOMBRE+ "'"+" and CONTACTO = " + CONTACTO 
      		 			+" and especialidad = '" + especialidad+ "'"+" and TARIFAS = '" + TARIFAS+ "'"+" and DESCRIPCION = '" + DESCRIPCION+ "'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	
	            while (resultSet.next()) {
	            	
	            	
	            	codigo=resultSet.getInt("Id_restaurante");
	                
	            }

} catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
}
	 return codigo;
	 
}

//.............................................FIN codigo para obtenrer id restaurante-....................................


//...........................................metodo para obtener todos los restaurantes que hay la BD..............................//

public String[] getValuesRestaurante()
{
     String num[]=new String[20];
     int cont=0; 
     int tamany;
     try {
         String Query = "SELECT * FROM " + "RESTAURANTES";
         Statement st = Conexion.createStatement();
         java.sql.ResultSet resultSet;
         resultSet = st.executeQuery(Query);
    
         while (resultSet.next()) {
         	
 	        num[cont]=resultSet.getString("NOMBRE");
         	cont++;
         }

     } catch (SQLException ex) {
         JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
     }
     
     return num;
 }

//...........................................FIN metodo para obtener todos los restaurantes que hay la BD..............................//		 
			 

//..........................METODO QUE edita valores de la tabla del RESTAURANTE seleccionado .........................................................//

public void UpdatedeleteRecord_Restaurante(int id_restaurante,String nombre,int contacto,String especialidad,String tarifas,String horario,String descripcion) {
      try {
         
      	String Query = "UPDATE RESTAURANTES SET Nombre ='"+nombre+"',"+"Contacto="+contacto+","+"Especialidad='"+
      	                especialidad+"',"+"Tarifas='"+tarifas+"',"+"Horario='"+horario+"',"+"Descripcion='"+descripcion+"'"
      			       +" WHERE Id_restaurante="+id_restaurante;
      	
      	
          Statement st = Conexion.createStatement();
          st.executeUpdate(Query);

        
          
          JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


      } catch (SQLException ex) {
          System.out.println(ex.getMessage());
          JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
      }
}	  


//..................................................fin metodo de editar RESTAURANTE............................................//

//..........................METODO QUE edita datos de la tabla del hotel seleccionado .........................................................//

public void UpdatedeleteRecord_TenerRestaurante(int id_restaurante,int id_ciudad,String nombrePais,String DniAdmin) {
     try {
        
     	String Query = "UPDATE TENER_restaurante SET id_ciudad="+id_ciudad+","+"Nombre_pais='"+nombrePais+"',"+"Dni_admin='"+
     	                DniAdmin+"'"+" WHERE Id_restaurante="+id_restaurante;
     	
         				Statement st = Conexion.createStatement();
         				st.executeUpdate(Query);

       
         
         JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


     } catch (SQLException ex) {
         System.out.println(ex.getMessage());
         JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
     }
}	  


//..................................................fin metodo de editar hotel.............................................//

							
//.............................METODO QUE eliminar restaurante DE NUESTRA BASE DE DATOS......................................

public void deleteRecord_EliminarRestaurante(int ID) {
     try {
         String Query = "DELETE FROM " + "Restaurantes" + " WHERE Id_restaurante =" + ID;
         Statement st = Conexion.createStatement();
         st.executeUpdate(Query);

     } catch (SQLException ex) {
         System.out.println(ex.getMessage());
         JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
     }
 }

//............................. FIN METODO QUE eliminar restaurante de BASE DE DATOS......................................

//.............................METODO QUE eliminar restaurante DE la tabla tener _restaurante BASE DE DATOS......................................

public void deleteRecord_Eliminar_TenerRestaurante(int ID) {
   try {
       String Query = "DELETE FROM " + "tener_restaurante" + " WHERE Id_restaurante =" + ID;
       Statement st = Conexion.createStatement();
       st.executeUpdate(Query);

   } catch (SQLException ex) {
       System.out.println(ex.getMessage());
       JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
   }
}

//............................. FIN METODO QUE eliminar HOTEL de la tabla tener_hotel de BASE DE DATOS......................................



//METODOS RELACIONADOS CON LA TABLA DE Bares

//.............................................codigo para inserata restaurante-....................................

public void insertarBar(String NOMBRE, int CONTACTO, String ESPECIALIDAD, String HORARIO, int precio, String DESCRIPCION) {
			   
	   try {
			        String Query = "INSERT INTO " + "BARES" + " VALUES("
			        		+ "SEQ_Bares.NEXTVAL"+","
			                + "'"+ NOMBRE + "',"
			                +  CONTACTO + ","
			                + "'"+ ESPECIALIDAD + "',"
			                 + "'"+ HORARIO + "',"
			                   + precio + ","
			            	+ "'"+ DESCRIPCION + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
//.............................................FIN codigo para inserata bar-....................................

//.............................................codigo para buscar  id del restaurante-....................................

public int Obtener_IdBar(String NOMBRE, int CONTACTO, String especialidad, int horario, String DESCRIPCION) {
	    
	 int codigo=0;
	 
	 try {
    String Query = "SELECT Id_bar FROM " + "BARES" + " WHERE NOMBRE = '" + NOMBRE+ "'"+" and CONTACTO = " + CONTACTO 
   		 			+" and especialidad = '" + especialidad+ "'"+" and Horario = " + horario+" and DESCRIPCION = '" + DESCRIPCION+ "'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	
	            while (resultSet.next()) {
	            	
	            	
	            	codigo=resultSet.getInt("Id_bar");
	                
	            }

} catch (SQLException ex) {
     JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
}
	 return codigo;
	 
}
//.....................................................FIN codigo............................................


//...........................................metodo para obtener todos los BARES que hay la BD..............................//

public String[] getValuesBares()
{
     String num[]=new String[20];
     int cont=0; 
     int tamany;
     try {
         String Query = "SELECT * FROM " + "BARES";
         Statement st = Conexion.createStatement();
         java.sql.ResultSet resultSet;
         resultSet = st.executeQuery(Query);
    
         while (resultSet.next()) {
         	
 	        num[cont]=resultSet.getString("NOMBRE");
         	cont++;
         }

     } catch (SQLException ex) {
         JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
     }
     
     return num;
 }

//...........................................FIN metodo para obtener todas los BARES que hay la BD..............................//		 

//.............................................FIN codigo para obtenrer id restaurante-....................................


//..........................METODO QUE edita valores de la tabla del RESTAURANTE seleccionado .........................................................//

public void UpdatedeleteRecord_Bar(int id_bar,String nombre,int contacto,String especialidad,int precio,String horario,String descripcion) {
    try {
       
    	String Query = "UPDATE BARES SET Nombre ='"+nombre+"',"+"Contacto="+contacto+","+"Especialidad='"+
    	                especialidad+"',"+"Precio='"+precio+"',"+"Horario='"+horario+"',"+"Descripcion='"+descripcion+"'"
    			       +" WHERE Id_bar="+id_bar;
    	
    	
        Statement st = Conexion.createStatement();
        st.executeUpdate(Query);

      
        
        JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
    }
}	  


//..................................................fin metodo de editar RESTAURANTE............................................//

//..........................METODO QUE edita datos de la tabla del hotel seleccionado .........................................................//

public void UpdatedeleteRecord_TenerBar(int id_bar,int id_ciudad,String nombrePais,String DniAdmin) {
   try {
      
   	String Query = "UPDATE TENER_bar SET id_ciudad="+id_ciudad+","+"Nombre_pais='"+nombrePais+"',"+"Dni_admin='"+
   	                DniAdmin+"'"+" WHERE Id_bar="+id_bar;
   	
       				Statement st = Conexion.createStatement();
       				st.executeUpdate(Query);

     
       
       JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


   } catch (SQLException ex) {
       System.out.println(ex.getMessage());
       JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
   }
}	  


//..................................................fin metodo de editar hotel.............................................//

//.............................METODO QUE eliminar bar DE NUESTRA BASE DE DATOS......................................

public void deleteRecord_EliminarBar(int ID) {
   try {
       String Query = "DELETE FROM " + "Bares" + " WHERE Id_bar =" + ID;
       Statement st = Conexion.createStatement();
       st.executeUpdate(Query);

   } catch (SQLException ex) {
       System.out.println(ex.getMessage());
       JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
   }
}

//............................. FIN METODO QUE eliminar bares de BASE DE DATOS......................................

//.............................METODO QUE eliminar bar DE la tabla tener _bar BASE DE DATOS......................................

public void deleteRecord_Eliminar_TenerBar(int ID) {
 try {
     String Query = "DELETE FROM " + "tener_bar" + " WHERE Id_bar =" + ID;
     Statement st = Conexion.createStatement();
     st.executeUpdate(Query);

 } catch (SQLException ex) {
     System.out.println(ex.getMessage());
     JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
 }
}

//............................. FIN METODO QUE eliminar bar de la tabla tener_bar de BASE DE DATOS......................................


//METODOS RELACIONADOS CON LA TABLA DE servicios

//.............................................codigo para inserata restaurante-....................................

public void insertarServicio(String tipo, int CONTACTO, String HORARIO,String DESCRIPCION) {
			   
	   try {
			        String Query = "INSERT INTO " + "servicios" + " VALUES("
			        		+ "SEQ_servicios.NEXTVAL"+","
			                + "'"+ tipo + "',"
			                + "'"+ DESCRIPCION + "',"
			                + CONTACTO + ","
			            	+ "'"+ HORARIO+ "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
//.............................................FIN codigo para inserata bar-....................................

//.............................................codigo para buscar  id del restaurante-....................................

public int Obtener_IdServicio(String tipo, int CONTACTO,  int horario, String DESCRIPCION) {
	    
	 int codigo=0;
	 
	 try {
  String Query = "SELECT Id_servicio FROM " + "servicios" + " WHERE tipo = '" + tipo+ "'"+" and CONTACTO = " + CONTACTO 
 		 			+" and horario = '" + horario+ "'"+" and DESCRIPCION = '" + DESCRIPCION+ "'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	
	            while (resultSet.next()) {
	            	
	            	
	            	codigo=resultSet.getInt("Id_servicio");
	                
	            }

} catch (SQLException ex) {
   JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
}
	 return codigo;
	 
}
//.....................................................FIN codigo............................................

//...........................................metodo para obtener todos los entre que hay la BD..............................//

public String[] getValuesServicios()
{
   String num[]=new String[20];
   int cont=0; 
   int tamany;
   try {
       String Query = "SELECT * FROM " + "servicios";
       Statement st = Conexion.createStatement();
       java.sql.ResultSet resultSet;
       resultSet = st.executeQuery(Query);
  
       while (resultSet.next()) {
       	
	        num[cont]=resultSet.getString("Tipo");
       	cont++;
       }

   } catch (SQLException ex) {
       JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
   }
   
   return num;
}

//...........................................FIN metodo para obtener todas los entre que hay la BD..............................//		 


//..........................METODO QUE edita valores de la tabla del RESTAURANTE seleccionado .........................................................//

public void UpdatedeleteRecord_Servicos(int id_Ser,String tipo,String descripcion,int contacto,String horario) {
  try {
     
  	String Query = "UPDATE SERVICIOS SET Tipo ='"+tipo+"',"+"Contacto="+contacto+","+"Descripcion='"+descripcion+"',"+"Horario='"+horario+"'"
  			       +" WHERE Id_servicio="+id_Ser;
  	
  	
      Statement st = Conexion.createStatement();
      st.executeUpdate(Query);

    
      
      JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


  } catch (SQLException ex) {
      System.out.println(ex.getMessage());
      JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
  }
}	  


//..................................................fin metodo de editar servicio............................................//

//..........................METODO QUE edita datos de la tabla del servicio seleccionado .........................................................//

public void UpdatedeleteRecord_TenerServicos(int id_serv,int id_ciudad,String nombrePais,String DniAdmin) {
 try {
    
 	String Query = "UPDATE TENER_Servicios SET id_ciudad="+id_ciudad+","+"Nombre_pais='"+nombrePais+"',"+"Dni_admin='"+
 	                DniAdmin+"'"+" WHERE Id_servicio="+id_serv;
 	
     				Statement st = Conexion.createStatement();
     				st.executeUpdate(Query);

   
     
     JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


 } catch (SQLException ex) {
     System.out.println(ex.getMessage());
     JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
 }
}	  


//..................................................fin metodo de editar servicios.............................................//


//.............................METODO QUE eliminar bar DE NUESTRA BASE DE DATOS......................................

public void deleteRecord_EliminarServicio(int ID) {
 try {
     String Query = "DELETE FROM " + "SERVICIOS" + " WHERE Id_servicio =" + ID;
     Statement st = Conexion.createStatement();
     st.executeUpdate(Query);

 } catch (SQLException ex) {
     System.out.println(ex.getMessage());
     JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
 }
}

//............................. FIN METODO QUE eliminar servicios de BASE DE DATOS......................................

//.............................METODO QUE eliminar bar DE la tabla tener _servicio BASE DE DATOS......................................

public void deleteRecord_Eliminar_TenerServicio(int ID) {
try {
   String Query = "DELETE FROM " + "tener_servicios" + " WHERE Id_servicio =" + ID;
   Statement st = Conexion.createStatement();
   st.executeUpdate(Query);

} catch (SQLException ex) {
   System.out.println(ex.getMessage());
   JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
}
}

//............................. FIN METODO QUE eliminar bar de la tabla tener_bar de BASE DE DATOS......................................


//METODOS RELACIONADOS CON LA TABLA DE Entretenimiento

//.............................................codigo para inserata restaurante-....................................

public void insertarEntre(String tipo, int CONTACTO, String HORARIO,String DESCRIPCION) {
			   
	   try {
			        String Query = "INSERT INTO " + "Entretenimiento" + " VALUES("
			        		+ "SEQ_Entretenimiento.NEXTVAL"+","
			                + "'"+ tipo + "',"
			                + "'"+ DESCRIPCION + "',"
			                + "'"+HORARIO + "',"
			            	+ CONTACTO+ ")";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
//.............................................FIN codigo para inserata bar-....................................

//.............................................codigo para buscar  id del restaurante-....................................

public int Obtener_IdEntre(String tipo, int CONTACTO,  String horario, String DESCRIPCION) {
	    
	 int codigo=0;
	 
	 try {
String Query = "SELECT Id_entre FROM " + "ENTRETENIMIENTO" + " WHERE tipo = '" + tipo+ "'"+" and CONTACTO = " + CONTACTO 
		 			+" and horario = '" + horario+ "'"+" and DESCRIPCION = '" + DESCRIPCION+ "'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	
	            while (resultSet.next()) {
	            	
	            	
	            	codigo=resultSet.getInt("Id_entre");
	                
	            }

} catch (SQLException ex) {
 JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
}
	 return codigo;
	 
}
//.....................................................FIN codigo............................................


//...........................................metodo para obtener todos los entre que hay la BD..............................//

public String[] getValuesEntre()
{
 String num[]=new String[20];
 int cont=0; 
 int tamany;
 try {
     String Query = "SELECT * FROM " + "Entretenimiento";
     Statement st = Conexion.createStatement();
     java.sql.ResultSet resultSet;
     resultSet = st.executeQuery(Query);

     while (resultSet.next()) {
     	
	        num[cont]=resultSet.getString("Tipo");
     	cont++;
     }

 } catch (SQLException ex) {
     JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
 }
 
 return num;
}

//...........................................FIN metodo para obtener todas los entre que hay la BD..............................//		 



//..........................METODO QUE edita valores de la tabla del RESTAURANTE seleccionado .........................................................//

public void UpdatedeleteRecord_Entre(int id_Ser,String tipo,String descripcion,int contacto,String horario) {
try {
   
	String Query = "UPDATE Entretenimiento SET Tipo ='"+tipo+"',"+"Contacto="+contacto+","+"Descripcion='"+descripcion+"',"+"Horario='"+horario+"'"
			       +" WHERE Id_entre="+id_Ser;
	
	
    Statement st = Conexion.createStatement();
    st.executeUpdate(Query);

  
    
    JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


} catch (SQLException ex) {
    System.out.println(ex.getMessage());
    JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
}
}	  


//..................................................fin metodo de editar servicio............................................//

//..........................METODO QUE edita datos de la tabla del servicio seleccionado .........................................................//

public void UpdatedeleteRecord_TenerEntre(int id_entre,int id_ciudad,String nombrePais,String DniAdmin) {
try {
  
	String Query = "UPDATE Tener_entretenimiento SET id_ciudad="+id_ciudad+","+"Nombre_pais='"+nombrePais+"',"+"dni_admin='"+
	                DniAdmin+"'"+" WHERE Id_entre="+id_entre;
	
   				Statement st = Conexion.createStatement();
   				st.executeUpdate(Query);

 
   
   JOptionPane.showMessageDialog(null, "Se ha modificado los datos");


} catch (SQLException ex) {
   System.out.println(ex.getMessage());
   JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
}
}	  


//..................................................fin metodo de editar servicios.............................................//



//.............................METODO QUE eliminar ENTRETENIMIENTO DE NUESTRA BASE DE DATOS......................................

public void deleteRecord_EliminarEntre(int ID) {
try {
   String Query = "DELETE FROM " + "Entretenimiento" + " WHERE Id_entre =" + ID;
   Statement st = Conexion.createStatement();
   st.executeUpdate(Query);

} catch (SQLException ex) {
   System.out.println(ex.getMessage());
   JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
}
}

//............................. FIN METODO QUE eliminar ENTRE. de BASE DE DATOS......................................

//.............................METODO QUE eliminar bar DE la tabla tener _ENTER BASE DE DATOS......................................

public void deleteRecord_Eliminar_TenerEntre(int ID) {
try {
 String Query = "DELETE FROM " + "tener_ENTRETENIMIENTO" + " WHERE Id_ENTRE =" + ID;
 Statement st = Conexion.createStatement();
 st.executeUpdate(Query);

} catch (SQLException ex) {
 System.out.println(ex.getMessage());
 JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
}
}

//............................. FIN METODO QUE eliminar bar de la tabla tener_ENTER de BASE DE DATOS......................................


// metodos relacionados con las consultas que se hacen la tabla de Administrados
	
	// .............................................obtener el nombre del usuario para identificarse a hora de hacer alguna cosa............................
					  
					  public String[] getValues_NombreAdministrador()
					  {
					        String num[]=new String[20];
					        int cont=0; 
					        int tamany;
					        try {
					            String Query = "SELECT * FROM " + "Administradores";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            
					            //tamany=resultSet.getRow();
					            while (resultSet.next()) {
					            	
					    	        num[cont]=resultSet.getString("Dni");
					            	cont++;

					            }

					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
					        }
					        
					        return num;
					    }
//.....................................................fin del codigo del obtener el admin................................................................
	// .............................................obtener el nombre del usuario para identificarse a hora de hacer alguna cosa............................
					  
	 public String[] getValues_usuaroAdministrador()
					  {
					        String num[]=new String[20];
					        int cont=0; 
					        int tamany;
					        try {
					            String Query = "SELECT * FROM " + "Administradores";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            
					            //tamany=resultSet.getRow();
					            while (resultSet.next()) {
					            	
					    	        num[cont]=resultSet.getString("USUARIO");
					            	cont++;

					            }

					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
					        }
					        
					        return num;
					    }
	//.......................................fin del codigo del obtener el admin................................................................
					    
	//...............................metodo para obtener el dni del administrado..............................
					  

					  
	 public String Obtener_Dni_Admin(String usuario)
		{
					       String dni="";

				 try {
					    String Query = "SELECT Dni FROM " + "Administradores" +" WHERE USUARIO='"+usuario+"'";
					    Statement st = Conexion.createStatement();
					    java.sql.ResultSet resultSet;
					   resultSet = st.executeQuery(Query);
					            
					            //tamany=resultSet.getRow();
					    while (resultSet.next()) {
					            	
					     dni=resultSet.getString("Dni");

			      }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			       }
					        
			  return dni;
	}
					  
					  
	//............................................FIN metodo de obtener el dni de el admin....................................
		 
					  
 //METODO QUE INSERTA VALORES de la tabla Opinar_Restaurante EN NUESTRA BASE DE DATOS------------------
					  
	 public void insertData_OpinarRestaurante( int Id_restaurante, String Usuario, String Clave, String Opinar)
	 {
							 try {
						        	
						            String Query = "INSERT INTO " + "Opinar_Restuarante" + " VALUES("
						            		+"SEQ_Opinar_Resta.NEXTVAL"+ ","
						            		+ Id_restaurante+"," 
						            		+ "'"+ Usuario + "',"
						            		+ "'"+ Clave + "',"
						            		+ "'"+ Opinar+ "',"
						            		+ "'"+ Fechapc()+ "')";
						            Statement st = Conexion.createStatement();
						            st.executeUpdate(Query);
						            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
						        } catch (SQLException ex) {
						            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
						        }
		 }					  
//-------------------------------------------FIN CODIGO.......................................................

 //METODO QUE INSERTA VALORES de la tabla Opinar_hotel EN NUESTRA BASE DE DATOS------------------
						  
	 public void insertData_OpinarHotel( int Id_hotel, String Usuario, String Clave, String Opinar) {
						     System.out.println(Id_hotel+" "+Usuario+" "+Clave+" "+Opinar+" "+Fechapc());   
							 try {
						        	
						            String Query = "INSERT INTO " + "Opinar_hotel" + " VALUES("
						            		+"SEQ_Opinar_Hoteles.NEXTVAL"+ ","
						            		+ Id_hotel+ "," 
						            		+ "'"+ Usuario + "',"
						            		+ "'"+ Clave + "',"
						            		+ "'"+ Opinar+ "',"
						            		+ "'"+ Fechapc()+ "')";
						            Statement st = Conexion.createStatement();
						            st.executeUpdate(Query);
						            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
						        } catch (SQLException ex) {
						            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
						        }
						    }					  
//-------------------------------------------FIN CODIGO.......................................................
	 
	//METODO QUE INSERTA VALORES de la tabla Opinar_bar EN NUESTRA BASE DE DATOS------------------
	  
public void insertData_OpinarBar( int Id_bar, String Usuario, String Clave, String Opinar) {
	
	System.out.println("  "+Id_bar+" "+Usuario+" "+Clave+" "+Opinar);
								 try {
							        	
							            String Query = "INSERT INTO " + "Opinar_bar" + " VALUES("
							            		+"SEQ_Opinar_Bar.NEXTVAL"+ ","
							            		+ Id_bar+ "," 
							            		+ "'"+ Usuario + "',"
							            		+ "'"+ Clave + "',"
							            		+ "'"+ Opinar+ "',"
							            		+ "'"+ Fechapc()+ "')";
							            Statement st = Conexion.createStatement();
							            st.executeUpdate(Query);
							            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
							        } catch (SQLException ex) {
							            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
							        }
							    }					  
	//-------------------------------------------FIN CODIGO.......................................................
		 
	//METODO QUE INSERTA VALORES de la tabla Opinar_bar EN NUESTRA BASE DE DATOS------------------
		  
	 public void insertData_OpinarServicio( int id_servicio, String Usuario, String Clave, String Opinar) {
								 try {
							        	
							            String Query = "INSERT INTO " + "Opinar_servicios" + " VALUES("
							            		+"SEQ_OpinarServicio.NEXTVAL"+ ","
	            								+ id_servicio+ "," 
							            		+ "'"+ Usuario + "',"
							            		+ "'"+ Clave + "',"
							            		+ "'"+ Opinar+ "',"
							            		+ "'"+ Fechapc()+ "')";
							            Statement st = Conexion.createStatement();
							            st.executeUpdate(Query);
							            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
							        } catch (SQLException ex) {
							            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
							        }
							    }					  
	//-------------------------------------------FIN CODIGO.......................................................
	 
	//METODO QUE INSERTA VALORES de la tabla Opinar_bar EN NUESTRA BASE DE DATOS------------------
	  
		 public void insertData_OpinarEntre( int id_entre, String Usuario, String Clave, String Opinar) {
									 try {
								        	
								            String Query = "INSERT INTO " + "Opinar_entretenimiento" + " VALUES("
								            		+"SEQ_Opinar_entre.NEXTVAL"+ ","
		            								+ id_entre+ "," 
								            		+ "'"+ Usuario + "',"
								            		+ "'"+ Clave + "',"
								            		+ "'"+ Opinar+ "',"
								            		+ "'"+ Fechapc()+ "')";
								            Statement st = Conexion.createStatement();
								            st.executeUpdate(Query);
								            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
								        } catch (SQLException ex) {
								            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
								        }
								    }					  
		//-------------------------------------------FIN CODIGO.......................................................
	// metodo para obtener la fecha del pc
		public String Fechapc()
			{
					Calendar c;
					c=Calendar.getInstance();
					int d=c.get(Calendar.DATE), m=1+(c.get(Calendar.MONTH)), a=c.get(Calendar.YEAR);
								
					String fecha=d+"/"+m+"/"+a;
					return fecha;
			}
							
}
