package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import Bares.GestionBar;
import Ciudad.Gestion_Ciudad_App;
import Entretenimiento.GestionEntretenimiento;
import Entretenimiento.MostrarEntre_App;
import Forum.Forum_App;
import Hoteles.GestionHotel;
import Main_Comun_App.Pagina_Principal;
import Main_Comun_App.SQL;
import Pais.GestionPais;
import Restaurantes.GestionRestaurante;
import Servicios.GestionServicios;
import Usuario.Registrar_Usuario_App;

import java.awt.Font;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.LineBorder;

public class AdministradorControlaTodo extends JFrame {

	private JPanel contentPane;
    public JFrame frame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministradorControlaTodo frame = new AdministradorControlaTodo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdministradorControlaTodo() {
		setResizable(false);
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.controlLtHighlight);
		panel.setBounds(0, 0, 784, 562);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(115, 131, 547, 383);
		panel.add(panel_3);
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel_3.setLayout(null);
		
		JLabel lblGestionarVentanas = new JLabel("Gestionar Ventanas");
		lblGestionarVentanas.setBounds(181, 11, 185, 27);
		lblGestionarVentanas.setFont(new Font("Arial", Font.BOLD, 18));
		panel_3.add(lblGestionarVentanas);
		
		JButton btnBares = new JButton("Bares");
		btnBares.setBounds(54, 102, 89, 48);
		panel_3.add(btnBares);
		
		JButton btnCiudad = new JButton("Ciudades");
		btnCiudad.setBounds(153, 102, 89, 48);
		panel_3.add(btnCiudad);
		
		JButton btnEntretenimiento = new JButton("Entretenimiento");
		btnEntretenimiento.setBounds(371, 161, 131, 48);
		panel_3.add(btnEntretenimiento);
		
		JButton btnHoteles = new JButton("Hoteles");
		btnHoteles.setBounds(252, 161, 109, 48);
		panel_3.add(btnHoteles);
		
		JButton btnPais = new JButton("Paises");
		btnPais.setBounds(153, 161, 89, 48);
		panel_3.add(btnPais);
		
		JButton btnRestaurantes = new JButton("Restaurantes");
		btnRestaurantes.setBounds(371, 103, 131, 47);
		panel_3.add(btnRestaurantes);
		
		JButton btnServicios = new JButton("Servicios");
		btnServicios.setBounds(54, 161, 89, 48);
		panel_3.add(btnServicios);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(252, 102, 109, 48);
		panel_3.add(btnRegistrar);
		
		JLabel lblAdmin = DefaultComponentFactory.getInstance().createTitle("Admin");
		lblAdmin.setForeground(Color.WHITE);
		lblAdmin.setFont(new Font("Arial", Font.BOLD, 16));
		lblAdmin.setBounds(141, 21, 88, 14);
		panel.add(lblAdmin);
		
		JLabel lblCuenta = DefaultComponentFactory.getInstance().createTitle("Cuenta");
		lblCuenta.setForeground(Color.WHITE);
		lblCuenta.setBounds(141, 37, 88, 14);
		panel.add(lblCuenta);
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 12));
		
		//BOTON ATRAS
		JLabel btnAtras = new JLabel("");
		btnAtras.setIcon(new ImageIcon(AdministradorCuenta.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setBounds(710, 92, 51, 51);
		panel.add(btnAtras);
		
		//TEXTO "JOURNEY & EXPERIENCE"
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Journey & Experience");
		lblNewJgoodiesTitle.setForeground(Color.WHITE);
		lblNewJgoodiesTitle.setBounds(539, 11, 222, 32);
		panel.add(lblNewJgoodiesTitle);
		lblNewJgoodiesTitle.setFont(new Font("Arial", Font.BOLD, 18));
		
		//IMAGEN FONDO
		//BACKGROUND
		JLabel ImagenFondo = new JLabel("");
		ImagenFondo.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/wallpaper.jpg")));
		ImagenFondo.setBounds(0, 0, 784, 562);
		panel.add(ImagenFondo);
		
		
		//LISTENER DEL BOTON BARES
		btnBares.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionBar ver = new GestionBar();
		        ver.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON CIUDADES
		btnCiudad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Gestion_Ciudad_App ver = new Gestion_Ciudad_App();
		        ver.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON ENTRETENIMIENTO
		btnEntretenimiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionEntretenimiento v = new GestionEntretenimiento();
		        v.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON RESTAURANTES
		btnRestaurantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionRestaurante ver = new GestionRestaurante();
		        ver.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON SERVICIOS
		btnServicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionServicios ver = new GestionServicios();
		        ver.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON PAISES
		btnPais.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionPais ver = new GestionPais();
		        ver.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON HOTELES
		btnHoteles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionHotel ver = new GestionHotel();
		        ver.setVisible(true);
		        
			}
		});
		
		//LISTENER DEL BOTON REGISTRAR USUARIO
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Registrar_Usuario_App ver = new Registrar_Usuario_App();
		        ver.setVisible(true);
		        
			}
		});
	
		//LISTENER DE LABEL ATRAS
		MouseListener patras =new MouseListener() {
			  
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
					AdministradorPantallaPrincipal v= new AdministradorPantallaPrincipal();
					v.setVisible(true);
					dispose();
				   
				   }
			};
			btnAtras.addMouseListener(patras);
			
			
			
			//LISTENER DE LABEL CUENTA
			MouseListener cuenta =new MouseListener() {
				   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
					   AdministradorCuenta cuenta=new AdministradorCuenta();
					   cuenta.setVisible(true);				   }
				};
				lblCuenta.addMouseListener(cuenta);
	}
}
