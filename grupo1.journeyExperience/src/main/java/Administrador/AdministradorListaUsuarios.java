package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import Main_Comun_App.SQL;

import java.awt.Font;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.SystemColor;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JTextField;

public class AdministradorListaUsuarios extends JFrame {
    
	private static Connection Conexion = null;
	private JPanel contentPane;
	public JLabel usuario;
	private JTextField listaUsuarios;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministradorListaUsuarios frame = new AdministradorListaUsuarios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public AdministradorListaUsuarios() {
		
				
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.text);
		panel.setBounds(0, 0, 784, 562);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblCuenta = DefaultComponentFactory.getInstance().createTitle("Cuenta");
		lblCuenta.setForeground(Color.WHITE);
		lblCuenta.setBounds(141, 37, 88, 14);
		panel.add(lblCuenta);
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 12));
		
		
		JLabel lblAdmin = DefaultComponentFactory.getInstance().createTitle("Admin");
		lblAdmin.setForeground(Color.WHITE);
		lblAdmin.setFont(new Font("Arial", Font.BOLD, 16));
		lblAdmin.setBounds(141, 21, 88, 14);
		panel.add(lblAdmin);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(UIManager.getBorder("RadioButton.border"));
		panel_1.setBounds(65, 76, 635, 486);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		
		JLabel lblListaDeUsuarios = DefaultComponentFactory.getInstance().createTitle("Lista de usuarios:");
		lblListaDeUsuarios.setFont(new Font("Arial", Font.BOLD, 17));
		lblListaDeUsuarios.setBounds(10, 11, 149, 14);
		panel_1.add(lblListaDeUsuarios);
		
		usuario = DefaultComponentFactory.getInstance().createLabel("");
		usuario.setFont(new Font("Arial", Font.BOLD, 12));
		usuario.setBounds(88, 108, 80, 14);
		panel_1.add(usuario);
		
		JLabel usuario2 = DefaultComponentFactory.getInstance().createLabel("Usuario2");
		usuario2.setFont(new Font("Arial", Font.BOLD, 12));
		usuario2.setBounds(92, 150, 88, 14);
		panel_1.add(usuario2);
		
		JLabel usuario3 = DefaultComponentFactory.getInstance().createLabel("Usuario3");
		usuario3.setFont(new Font("Arial", Font.BOLD, 12));
		usuario3.setBounds(92, 191, 88, 14);
		panel_1.add(usuario3);
		
		JButton btnVer =  new JButton("Ver");
		btnVer.setBackground(new Color(255, 250, 250));
		btnVer.setBounds(412, 105, 61, 23);
		panel_1.add(btnVer);
		
		JButton btnVer2 = new JButton("Ver");
		btnVer2.setBackground(new Color(255, 250, 250));
		btnVer2.setBounds(412, 147, 61, 23);
		panel_1.add(btnVer2);
		
		JButton btnVer3 = new JButton("Ver");
		btnVer3.setForeground(new Color(0, 0, 0));
		btnVer3.setBackground(new Color(255, 250, 250));
		btnVer3.setBounds(412, 188, 61, 23);
		panel_1.add(btnVer3);
		
		JLabel nUsuario = DefaultComponentFactory.getInstance().createTitle("Usuarios");
		nUsuario.setFont(new Font("Arial", Font.BOLD, 12));
		nUsuario.setBounds(96, 84, 58, 14);
		panel_1.add(nUsuario);
		
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Journey & Experience");
		lblNewJgoodiesTitle.setForeground(Color.WHITE);
		lblNewJgoodiesTitle.setBounds(473, 11, 217, 32);
		panel.add(lblNewJgoodiesTitle);
		lblNewJgoodiesTitle.setFont(new Font("Arial", Font.BOLD, 20));
		
		JLabel lblRolActual = new JLabel("Rol Actual");
		lblRolActual.setFont(new Font("Arial", Font.BOLD, 12));
		lblRolActual.setBounds(238, 82, 58, 14);
		panel_1.add(lblRolActual);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Admin");
		rdbtnNewRadioButton.setBackground(Color.WHITE);
		rdbtnNewRadioButton.setEnabled(false);
		rdbtnNewRadioButton.setFont(new Font("Arial", Font.PLAIN, 12));
		rdbtnNewRadioButton.setBounds(163, 104, 61, 23);
		panel_1.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("SuperUser");
		rdbtnNewRadioButton_1.setBackground(Color.WHITE);
		rdbtnNewRadioButton_1.setEnabled(false);
		rdbtnNewRadioButton_1.setFont(new Font("Arial", Font.PLAIN, 12));
		rdbtnNewRadioButton_1.setBounds(230, 104, 88, 23);
		panel_1.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Estandar");
		rdbtnNewRadioButton_2.setBackground(Color.WHITE);
		rdbtnNewRadioButton_2.setSelected(true);
		rdbtnNewRadioButton_2.setEnabled(false);
		rdbtnNewRadioButton_2.setFont(new Font("Arial", Font.PLAIN, 12));
		rdbtnNewRadioButton_2.setBounds(322, 103, 84, 23);
		panel_1.add(rdbtnNewRadioButton_2);
		
		JRadioButton radioButton = new JRadioButton("SuperUser");
		radioButton.setBackground(Color.WHITE);
		radioButton.setEnabled(false);
		radioButton.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton.setBounds(230, 146, 88, 23);
		panel_1.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("Admin");
		radioButton_1.setBackground(Color.WHITE);
		radioButton_1.setEnabled(false);
		radioButton_1.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_1.setBounds(163, 146, 61, 23);
		panel_1.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton("Estandar");
		radioButton_2.setBackground(Color.WHITE);
		radioButton_2.setSelected(true);
		radioButton_2.setEnabled(false);
		radioButton_2.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_2.setBounds(322, 145, 84, 23);
		panel_1.add(radioButton_2);
		
		JRadioButton radioButton_3 = new JRadioButton("SuperUser");
		radioButton_3.setBackground(Color.WHITE);
		radioButton_3.setEnabled(false);
		radioButton_3.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_3.setBounds(230, 187, 88, 23);
		panel_1.add(radioButton_3);
		
		JRadioButton radioButton_4 = new JRadioButton("Admin");
		radioButton_4.setBackground(Color.WHITE);
		radioButton_4.setEnabled(false);
		radioButton_4.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_4.setBounds(163, 187, 61, 23);
		panel_1.add(radioButton_4);
		
		JRadioButton radioButton_5 = new JRadioButton("Estandar");
		radioButton_5.setBackground(Color.WHITE);
		radioButton_5.setSelected(true);
		radioButton_5.setEnabled(false);
		radioButton_5.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_5.setBounds(322, 186, 84, 23);
		panel_1.add(radioButton_5);
		
		JLabel lblOpciones = new JLabel("Opciones");
		lblOpciones.setFont(new Font("Arial", Font.BOLD, 12));
		lblOpciones.setBounds(447, 80, 61, 14);
		panel_1.add(lblOpciones);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBackground(new Color(255, 250, 250));
		btnBorrar.setBounds(483, 105, 72, 23);
		panel_1.add(btnBorrar);
		
		JButton button_1 = new JButton("Borrar");
		button_1.setBackground(new Color(255, 250, 250));
		button_1.setBounds(483, 147, 72, 23);
		panel_1.add(button_1);
		
		JButton button_2 = new JButton("Borrar");
		button_2.setBackground(new Color(255, 250, 250));
		button_2.setBounds(483, 188, 72, 23);
		panel_1.add(button_2);
		
		//JTEXTarea-
		JTextArea areaTexto = new JTextArea();
		areaTexto.setBounds(212,298,141,117);
		areaTexto.setWrapStyleWord(true);
		areaTexto.setLineWrap(true);
		panel_1.add(areaTexto);
		
		JScrollPane rueda = new JScrollPane(areaTexto);
		rueda.setBounds(120,266,337,117);
		panel_1.add(rueda);
	
		SQL db=new SQL();
		//LLAMAMOS LOS VALORES DE LA BASE DE DATOS A LOS TEXTFIELD----------------------------------
		try {
			db.SQLConnection("grupo1","root","");
		    
			usuario.setText(db.getValuesUsuario("arkantoxs")[0]);
			usuario2.setText(db.getValuesUsuario("dahakaxs")[0]);
			usuario3.setText(db.getValuesUsuario("respawn")[0]);
			

		
			db.closeConnection();
			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}		
		//-------------------------------------------------------------------------------------------
		
		
		//BOTON ATRAS
		JLabel btnAtras = new JLabel("");
		btnAtras.setIcon(new ImageIcon(AdministradorCuenta.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setBounds(710, 92, 51, 51);
		panel.add(btnAtras);
		
		//IMAGEN FONDO
		
		JLabel ImagenFondo = new JLabel("");
		ImagenFondo.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/wallpaper.jpg")));
		ImagenFondo.setBounds(0, 0, 784, 562);
		panel.add(ImagenFondo);
		
		//LISTENER DE LABEL ATRAS
		MouseListener patras =new MouseListener() {
			  
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
					AdministradorPantallaPrincipal v= new AdministradorPantallaPrincipal();
					v.setVisible(true);
					dispose();
				   
				   }
			};
			btnAtras.addMouseListener(patras);
		//LISTENER BOTON VER
		
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			
			AdministradorVerUsuario ver = new AdministradorVerUsuario();
			ver.setVisible(true);
			dispose();
			
			}
		});
		
		//LISTENER BOTON VER 2
		
		btnVer2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			AdministradorVerUsuario2 ver = new AdministradorVerUsuario2();
			ver.setVisible(true);
			dispose();
			
			}
		});
		
		//LISTENER BOTON VER
		
		btnVer3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
			
			AdministradorVerUsuario3 ver = new AdministradorVerUsuario3();
			ver.setVisible(true);
			dispose();
			
			}
		});
		
		
		//LISTENER DE LABEL CUENTA
		MouseListener cuenta =new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   AdministradorCuenta cuenta=new AdministradorCuenta();
				   cuenta.setVisible(true);				   }
			};
			lblCuenta.addMouseListener(cuenta);
	}
	}

