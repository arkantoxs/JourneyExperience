package Pais;

import java.awt.BorderLayout;
import WipAssiya.SQL;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;

public class EliminarPaisApp extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarPaisApp frame = new EliminarPaisApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public EliminarPaisApp() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 510, 427);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(21, 64, 312, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(EliminarPaisApp.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(335, 31, 143, 76);
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Eliminar el pais");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(102, 183, 134, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Pais");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(140, 239, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(215, 236, 118, 20);
		contentPane.add(comboBox);
		
		JButton btnNewButton = new JButton("Eliminar");
		
		btnNewButton.setBounds(254, 292, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(EliminarPaisApp.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(59, 285, 68, 47);
		contentPane.add(btnAtras);
		
//....... codigo para a�adir paises k hay en BD en comboBox para que el admin. seleccione el pais que quiere eliminar.........
		
				SQL bd=new SQL();
				
				try {
					bd.SQLConnection("grupo1","root","");
					String rellenar[]=bd.getValuesPais();
					
					for(int i=0;i<rellenar.length;i++)
					{
						comboBox.addItem(rellenar[i]);		
					}
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

//..............................fin de codigo de a�adir paises a combox.............................................................				
				
//.................................... borrar de base de datos el pais seleccionado.............................................
				
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						SQL bd=new SQL();
						
						try {
							bd.SQLConnection("grupo1","root","");
				            String Paisseleccionado=comboBox.getSelectedItem().toString();
					        bd.deleteRecord_Pais(Paisseleccionado);
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
					}
				});

//............................................. fin de evento para eliminar pais selecionado de BD............................................//
	
//............................................. evento a clicar sobre label,atras............................................//
				
				 MouseListener clikLabel2=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
					 
					   setVisible(false);  //CERRAMOS LA VENTANA DE ELIMINAR

					   GestionPais atras=new  GestionPais();
					   atras.setVisible(true);				   }
				};
				btnAtras.addMouseListener(clikLabel2);
				 
//............................................. fin evento a clicar sobre label,atras............................................//
	}
}
