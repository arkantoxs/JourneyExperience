package Pais;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import WipAssiya.SQL;
import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;

public class Mostrar_Paises_App extends JFrame {

	private JPanel contentPane;
	private JLabel lblNewLabel;
	private JTextField NombrePais;
	private JComboBox ListaPais;
	private JTextField CapitalPais;
	private JTextField ContinentePais;
	private JTextField HabitantePais;
	private JTextField IdiomaPais;
	private JTextField MonedaPais;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	public String usuarios="";
	public String claves="";


	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mostrar_Paises_App frame = new Mostrar_Paises_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Mostrar_Paises_App() {
		setTitle("Datos del pais");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 581, 628);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(71, 47, 364, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Mostrar_Paises_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(411, 29, 134, 76);
		contentPane.add(label_1);
		
		final JLabel lblNewLabel = new JLabel("Datos del pais selecionado");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel.setBounds(22, 263, 169, 14);
		contentPane.add(lblNewLabel);
		lblNewLabel.setVisible(false);
		
		
		final JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(108, 296, 67, 14);
		contentPane.add(lblNewLabel_1);
		lblNewLabel_1.setVisible(false);
		
		final JLabel lblHabitantes = new JLabel("Habitantes");
		lblHabitantes.setFont(new Font("Arial", Font.BOLD, 11));
		lblHabitantes.setBounds(108, 372, 67, 14);
		contentPane.add(lblHabitantes);
		lblHabitantes.setVisible(false);

		
		final JLabel lblContinente = new JLabel("Continente");
		lblContinente.setFont(new Font("Arial", Font.BOLD, 11));
		lblContinente.setBounds(108, 347, 63, 14);
		contentPane.add(lblContinente);
		lblContinente.setVisible(false);
		
		final JLabel label_2 = new JLabel("Idioma");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(108, 397, 67, 14);
		contentPane.add(label_2);
		label_2.setVisible(false);
		
		final JLabel lblCapital = new JLabel("Capital");
		lblCapital.setFont(new Font("Arial", Font.BOLD, 11));
		lblCapital.setBounds(108, 322, 67, 14);
		contentPane.add(lblCapital);
		lblCapital.setVisible(false);
		
		final JLabel lblMoneda = new JLabel("Moneda");
		lblMoneda.setFont(new Font("Arial", Font.BOLD, 11));
		lblMoneda.setBounds(108, 422, 67, 14);
		contentPane.add(lblMoneda);
		lblMoneda.setVisible(false);
		
		final JComboBox ListaPais = new JComboBox();
		ListaPais.setBounds(220, 202, 151, 20);
		contentPane.add(ListaPais);
	
//..........................codigo para obtener los paises y a�adirlos a combox...........................................//

		SQL bd=new SQL();

		try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesPais();
		
				for(int i=0;i<rellenar.length;i++)
				{
					ListaPais.addItem(rellenar[i]);		
				}
		} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
		}

//..........................fin de codigo para obtener los paises y a�adirlos a combox...........................................//
		
		NombrePais = new JTextField();
		NombrePais.setEditable(false);
		NombrePais.setBounds(187, 293, 142, 20);
		contentPane.add(NombrePais);
		NombrePais.setColumns(10);
		NombrePais.setVisible(false);
		
		JLabel lblNewLabel_2 = new JLabel("Pais que quieres mostrar");
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_2.setBounds(22, 205, 169, 14);
		contentPane.add(lblNewLabel_2);
		
		CapitalPais = new JTextField();
		CapitalPais.setEditable(false);
		CapitalPais.setColumns(10);
		CapitalPais.setBounds(187, 319, 142, 20);
		contentPane.add(CapitalPais);
		CapitalPais.setVisible(false);
		
		ContinentePais = new JTextField();
		ContinentePais.setEditable(false);
		ContinentePais.setColumns(10);
		ContinentePais.setBounds(187, 344, 142, 20);
		contentPane.add(ContinentePais);
		ContinentePais.setVisible(false);
		
		HabitantePais = new JTextField();
		HabitantePais.setEditable(false);
		HabitantePais.setColumns(10);
		HabitantePais.setBounds(187, 369, 142, 20);
		contentPane.add(HabitantePais);
		HabitantePais.setVisible(false);
		
		IdiomaPais = new JTextField();
		IdiomaPais.setEditable(false);
		IdiomaPais.setColumns(10);
		IdiomaPais.setBounds(187, 394, 142, 20);
		contentPane.add(IdiomaPais);
		IdiomaPais.setVisible(false);
		
		MonedaPais = new JTextField();
		MonedaPais.setEditable(false);
		MonedaPais.setColumns(10);
		MonedaPais.setBounds(187, 419, 142, 20);
		contentPane.add(MonedaPais);
		MonedaPais.setVisible(false);
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setBounds(419, 201, 89, 23);
		contentPane.add(btnNewButton);
		
		final JLabel lblNewLabel_3 = new JLabel("Color de la");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_3.setBounds(108, 455, 67, 14);
		contentPane.add(lblNewLabel_3);
		lblNewLabel_3.setVisible(false);
		
		
		final JLabel lblBandera = new JLabel("    bandera");
		lblBandera.setFont(new Font("Arial", Font.BOLD, 11));
		lblBandera.setBounds(108, 474, 67, 14);
		contentPane.add(lblBandera);
		lblBandera.setVisible(false);
		
		final JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setText("");
		textArea.setBounds(187, 450, 142, 57);
		contentPane.add(textArea);
		
		JLabel principal = new JLabel("P\u00E1gina principal ");
		principal.setForeground(Color.BLACK);
		principal.setFont(new Font("Arial", Font.BOLD, 13));
		principal.setBounds(33, 134, 124, 23);
		contentPane.add(principal);
		
		JLabel lblForum = new JLabel("      Forum");
		lblForum.setForeground(Color.BLACK);
		lblForum.setFont(new Font("Arial", Font.BOLD, 13));
		lblForum.setBounds(231, 134, 111, 23);
		contentPane.add(lblForum);
		
		JLabel iniciar = new JLabel("Iniciar sesion");
		iniciar.setForeground(Color.BLACK);
		iniciar.setFont(new Font("Arial", Font.BOLD, 13));
		iniciar.setBounds(418, 134, 111, 23);
		contentPane.add(iniciar);
		textArea.setVisible(false);
		
		
	
//............................................. evento a clicar sobre label,atras............................................//
			
			 MouseListener clikLabel2=new MouseListener() {
					   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   setVisible(false);  //CERRAMOS LA VENTANA DE MOSTRAR

				   GestionPais atras=new  GestionPais();
				   atras.setVisible(true);				   }
			};
			 
//............................................. fin evento a clicar sobre label,atras............................................//
			 

// ......................................buscar y mostrar datos del pais a selecionado........................................//
			 

				btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
							 SQL bd=new SQL();
							 
							 try {
								 String pais= ListaPais.getSelectedItem().toString();
								 String QueryPais="SELECT * FROM " + "PAISES"+ " WHERE NOMBRE = '" + pais + "'";
								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryPais);
								 
								 while (ResultSet.next())
								 {
									 NombrePais.setText(ResultSet.getString("Nombre"));
									 CapitalPais.setText(ResultSet.getString("CAPITAL"));
									 ContinentePais.setText(ResultSet.getString("NOMBRE_CONT"));
									 HabitantePais.setText(ResultSet.getString("Habitantes"));
									 IdiomaPais.setText(ResultSet.getString("Idioma"));
									 MonedaPais.setText(ResultSet.getString("Moneda"));
									 textArea.setText(ResultSet.getString("Bandera"));
									 
									 
								 }
								 
								
							} catch (Exception ew) {
								// TODO Auto-generated catch block
								ew.printStackTrace();
							}
								lblNewLabel.setVisible(true);
								lblNewLabel_1.setVisible(true);
								lblHabitantes.setVisible(true);
								lblContinente.setVisible(true);
								label_2.setVisible(true);
								lblCapital.setVisible(true);
								lblMoneda.setVisible(true);
								lblNewLabel_3.setVisible(true);
								lblBandera.setVisible(true);
								NombrePais.setVisible(true);
								CapitalPais.setVisible(true);
								ContinentePais.setVisible(true);
								HabitantePais.setVisible(true);
								IdiomaPais.setVisible(true);
								MonedaPais.setVisible(true);
								textArea.setVisible(true);

						}
						

					});
			 
			 
//.................................................................fin....................................................//


	
				 //------------------------------------------------- Evento para ir al forum...............................................
				
				 MouseListener Forum=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   Forum_App PaginaForum=new  Forum_App(usuarios,claves);
						   PaginaForum.setVisible(true);				   }
					};
					lblForum.addMouseListener(Forum);
					    	
		//-------------------------------------------------FIN  Evento para ir al forum...............................................

					
					//------------------------------------pagina principal.................................................................
					 
					 MouseListener PaginaPrincipal=new MouseListener() {
							   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   Pagina_Principal atras=new  Pagina_Principal();
						   atras.setVisible(true);				   }
					};
					principal.addMouseListener(PaginaPrincipal);
					    	
					   
			//------------------------------------fin ................................................................
					 
					//LISTENER DE LABEL INICIAR SESION
					MouseListener iniSesion =new MouseListener() {
						   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Iniciar_Session_Usuario_App inicio = new Iniciar_Session_Usuario_App();
							   inicio.setVisible(true);
							   
							   }
						};
					iniciar.addMouseListener(iniSesion);				
				

}
}
