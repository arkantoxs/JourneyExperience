// Assiya Mrabet
package Usuario;

import java.awt.BorderLayout
;

import WipAssiya.SQL;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import javax.swing.JTextPane;

public class Registrar_Usuario_App extends JFrame {

	private JPanel contentPane;
	private JTextField CodigoPostal;
	private JTextField Apellido;
	private JTextField Nombre;
	private JTextField Email;
	private JTextField usuario;
	private JTextField viaje;
	private JPasswordField contra;
	private JTextField pais;
	public String usuarios="";
	public String claves="";

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registrar_Usuario_App frame = new Registrar_Usuario_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Registrar_Usuario_App() {
		setTitle("Registrarse");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 570, 587);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setBounds(33, 33, 364, 43);
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 30));
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(386, 13, 143, 99);
		label_1.setIcon(new ImageIcon(Registrar_Usuario_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Registrarse");
		lblNewLabel.setBounds(33, 112, 133, 22);
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 15));
		contentPane.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 13));
		lblNombre.setBounds(27, 165, 68, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido\r\n");
		lblApellido.setFont(new Font("Arial", Font.BOLD, 13));
		lblApellido.setBounds(27, 193, 68, 14);
		contentPane.add(lblApellido);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 13));
		lblEmail.setBounds(27, 225, 68, 14);
		contentPane.add(lblEmail);
		
		JLabel lblCodigoPostal = new JLabel("Codigo Postal");
		lblCodigoPostal.setFont(new Font("Arial", Font.BOLD, 13));
		lblCodigoPostal.setBounds(27, 255, 93, 22);
		contentPane.add(lblCodigoPostal);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Arial", Font.BOLD, 13));
		lblUsuario.setBounds(33, 437, 68, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Arial", Font.BOLD, 13));
		lblContrasea.setBounds(33, 468, 87, 14);
		contentPane.add(lblContrasea);
		
		CodigoPostal = new JTextField();
		CodigoPostal.setBounds(139, 254, 151, 20);
		contentPane.add(CodigoPostal);
		CodigoPostal.setColumns(10);
		
		Apellido = new JTextField();
		Apellido.setColumns(10);
		Apellido.setBounds(139, 191, 151, 20);
		contentPane.add(Apellido);
		
		Nombre = new JTextField();
		Nombre.setColumns(10);
		Nombre.setBounds(139, 163, 151, 20);
		contentPane.add(Nombre);
		
		Email = new JTextField();
		Email.setColumns(10);
		Email.setBounds(139, 223, 151, 20);
		contentPane.add(Email);
		
		usuario = new JTextField();
		usuario.setColumns(10);
		usuario.setBounds(139, 435, 151, 20);
		contentPane.add(usuario);
		
		viaje = new JTextField();
		viaje.setColumns(10);
		viaje.setBounds(139, 334, 151, 20);
		contentPane.add(viaje);
		
		JLabel lblNewLabel_1 = new JLabel("A qu\u00E9 pa\u00EDses \r\nest\u00E1  acostumbrado\r\n a viajar?");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 13));
		lblNewLabel_1.setBounds(27, 285, 278, 22);
		contentPane.add(lblNewLabel_1);
		
		contra = new JPasswordField();
		contra.setBounds(139, 466, 151, 20);
		contentPane.add(contra);
		
		JButton btnNewButton = new JButton("Registrarse");
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 11));
		
		btnNewButton.setBounds(373, 498, 101, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblltimoViaje = new JLabel("\u00FAltimo viaje ?");
		lblltimoViaje.setFont(new Font("Arial", Font.BOLD, 13));
		lblltimoViaje.setBounds(27, 336, 93, 14);
		contentPane.add(lblltimoViaje);
		
		pais = new JTextField();
		pais.setColumns(10);
		pais.setBounds(139, 303, 151, 20);
		contentPane.add(pais);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(Registrar_Usuario_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(52, 493, 68, 45);
		contentPane.add(btnAtras);
		
		JLabel lblOtrosDatos = new JLabel("Otros Datos");
		lblOtrosDatos.setFont(new Font("Arial", Font.BOLD, 11));
		lblOtrosDatos.setBounds(32, 375, 69, 14);
		contentPane.add(lblOtrosDatos);
		
		final JTextPane otros = new JTextPane();
		otros.setBounds(139, 365, 151, 59);
		contentPane.add(otros);
		
		JLabel lblNewLabel_5 = new JLabel("*\r\n");
		lblNewLabel_5.setForeground(Color.RED);
		lblNewLabel_5.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblNewLabel_5.setBounds(297, 165, 46, 14);
		contentPane.add(lblNewLabel_5);
		
		JLabel label_2 = new JLabel("*\r\n");
		label_2.setForeground(Color.RED);
		label_2.setFont(new Font("Arial Black", Font.PLAIN, 11));
		label_2.setBounds(297, 194, 46, 14);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("*\r\n");
		label_3.setForeground(Color.RED);
		label_3.setFont(new Font("Arial Black", Font.PLAIN, 11));
		label_3.setBounds(297, 226, 46, 14);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("*\r\n");
		label_4.setForeground(Color.RED);
		label_4.setFont(new Font("Arial Black", Font.PLAIN, 11));
		label_4.setBounds(297, 260, 46, 14);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("*\r\n");
		label_5.setForeground(Color.RED);
		label_5.setFont(new Font("Arial Black", Font.PLAIN, 11));
		label_5.setBounds(297, 438, 46, 14);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("*\r\n");
		label_6.setForeground(Color.RED);
		label_6.setFont(new Font("Arial Black", Font.PLAIN, 11));
		label_6.setBounds(297, 469, 46, 14);
		contentPane.add(label_6);
		
		JLabel lblDatosObligatorios = new JLabel("* Datos obligatorios");
		lblDatosObligatorios.setForeground(Color.RED);
		lblDatosObligatorios.setFont(new Font("Arial", Font.BOLD, 10));
		lblDatosObligatorios.setBounds(353, 166, 119, 14);
		contentPane.add(lblDatosObligatorios);
		lblDatosObligatorios.setVisible(false);
		
		
	//................................ evento a clicar sobre label,atras......................................................
		
				 MouseListener clikLabel=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
					   setVisible(false);  //CERRAMOS LA VENTANA DE registre

					   Iniciar_Session_Usuario_App atras=new  Iniciar_Session_Usuario_App();
					   atras.setVisible(true);	
					   setVisible(false);
					   
				 }
				};
				btnAtras.addMouseListener(clikLabel);
				
	//................................ FIN evento a clicar sobre label,atras......................................................
		
		// gguardar los datos
		// comprobar si, email, contras. y usuario ya estan en uso o no
		// si no estan en en uso se registra al usuario en caso tendra que introduzca otros nuevos
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        int Codigopostal=Integer.parseInt(CodigoPostal.getText());
		        usuarios=usuario.getText().toString();
		        claves=contra.getText().toString();

				SQL registrar=new SQL();
				try {
					registrar.SQLConnection("grupo1", "root", "");
					registrar.insertDataUsuarios(usuario.getText(),contra.getText(),Nombre.getText(),Apellido.getText(),
							Email.getText(),Codigopostal,pais.getText(),viaje.getText(),otros.getText());
					   EspacioPersonalApp personal=new EspacioPersonalApp(usuarios,claves);
					   //Iniciar
					   personal.setVisible(true);
					   

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		
	}
}
