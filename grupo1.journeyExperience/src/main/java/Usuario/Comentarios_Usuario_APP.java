//Assiya Mrabet
package Usuario;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;
import javax.swing.JList;


public class Comentarios_Usuario_APP extends JFrame {

	private JPanel contentPane;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private JLabel lblDescripcion;
	private JLabel lblNombre;
	public String usuarios="";
	public String claves="";
	public String nombres="";
	public String table="";
	public int codes=0;
	public String Opinar="";

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Comentarios_Usuario_APP frame = new Comentarios_Usuario_APP(usuarios,claves);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Comentarios_Usuario_APP(String usuario,String clave,String nombre,String nameTable,int cod,String opinartable) {
		setResizable(false);
		
		this.usuarios=usuario;
		this.claves=clave;
		this.nombres=nombre;
		this.table=nameTable;
		this.codes=cod;
		this.Opinar=opinartable;
		setTitle("Opini\u00F3n del usuario\r\n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 678);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 27));
		label.setBounds(74, 42, 342, 57);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Comentarios_Usuario_APP.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(463, 11, 142, 88);
		contentPane.add(label_1);
		
		JLabel lblDescripcion = new JLabel("Descripcion del restaurante");
		lblDescripcion.setBounds(181, 236, 333, 50);
		contentPane.add(lblDescripcion);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Comentarios_Usuario_APP.class.getResource("/Vistas/servicios.png")));
		lblNewLabel_1.setBounds(48, 198, 123, 100);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNombre = new JLabel("Restaurante X");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 12));
		lblNombre.setBounds(181, 211, 103, 14);
		contentPane.add(lblNombre);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(44, 188, 534, 121);
		contentPane.add(textPane);
		
		JLabel lblComentarios = new JLabel("Comentarios");
		lblComentarios.setFont(new Font("Arial", Font.BOLD, 11));
		lblComentarios.setBounds(58, 320, 111, 14);
		contentPane.add(lblComentarios);
		
		JButton btnComentar = new JButton("Comentar");
		btnComentar.setFont(new Font("Arial", Font.BOLD, 11));
		
		btnComentar.setBounds(463, 592, 105, 23);
		contentPane.add(btnComentar);
		
		JLabel lblDejaTuComentario = new JLabel("Deja tu comentario");
		lblDejaTuComentario.setFont(new Font("Arial", Font.BOLD, 12));
		lblDejaTuComentario.setBounds(71, 463, 121, 14);
		contentPane.add(lblDejaTuComentario);
		
		final JTextPane comentar = new JTextPane();
		comentar.setBounds(69, 488, 523, 91);
		contentPane.add(comentar);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(602, 177, 17, 451);
		contentPane.add(scrollBar);
		
		JLabel label_3 = new JLabel("P\u00E1gina principal ");
		label_3.setForeground(new Color(0, 0, 0));
		label_3.setFont(new Font("Arial", Font.BOLD, 13));
		label_3.setBounds(45, 143, 124, 23);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("Espacio personal");
		label_4.setForeground(new Color(0, 0, 0));
		label_4.setFont(new Font("Arial", Font.BOLD, 13));
		label_4.setBounds(239, 143, 124, 23);
		contentPane.add(label_4);
		
		JLabel lblCerrarSesion = new JLabel("Cerrar sessi\u00F3n");
		lblCerrarSesion.setForeground(new Color(0, 0, 0));
		lblCerrarSesion.setFont(new Font("Arial", Font.BOLD, 13));
		lblCerrarSesion.setBounds(450, 143, 124, 23);
		contentPane.add(lblCerrarSesion);
		
		DefaultListModel listaUsuarios= new DefaultListModel();
		JList listUsuarios = new JList(listaUsuarios);
		listUsuarios.setBounds(58, 345, 67, 88);
		contentPane.add(listUsuarios);
		
		DefaultListModel listaComentarios= new DefaultListModel();
		JList listComentarios = new JList(listaComentarios);
		listComentarios.setBounds(127, 345, 451, 88);
		contentPane.add(listComentarios);
		
		
		/* mostrar los hoteles con comentarios o restaurantes con comentarios o bar con comentarios 
		o servicios con comentarios o entretenimiento con comentarios segun lo k pida el usuario */
		
		
		
		SQL bd=new SQL();

		switch(table)
		{
		case "Bares":
		case "Hoteles":
		case "Restaurantes":
		
		try {
			bd.SQLConnection("grupo1","root","");
			
		
			String QueryHotel="SELECT * FROM " + table+ " WHERE nombre='"+nombres+"'";
			Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
			
			Statement st=Conexion.createStatement();
            java.sql.ResultSet ResultSet;ResultSet= st.executeQuery(QueryHotel);
			 
            while (ResultSet.next())
            {
            	lblNombre.setText(ResultSet.getString("Nombre"));
            	lblDescripcion.setText(ResultSet.getString("descripcion"));
            }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		break;
		case "Servicios":
		case "Entretenimiento":
			
			try {
				bd.SQLConnection("grupo1","root","");
				
			
				String QueryHotel="SELECT * FROM " + table+ " WHERE Tipo='"+nombres+"'";
				Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
				
				Statement st=Conexion.createStatement();
	            java.sql.ResultSet ResultSet;ResultSet= st.executeQuery(QueryHotel);
				 
	            while (ResultSet.next())
	            {
	            	lblNombre.setText(ResultSet.getString("Tipo"));
	            	lblDescripcion.setText(ResultSet.getString("descripcion"));
	            }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		}
			
			
			
		
		try {
			bd.SQLConnection("grupo1","root","");

			String QueryHotel="SELECT * FROM " + Opinar;
			Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
			
			Statement st=Conexion.createStatement();
            java.sql.ResultSet ResultSet;ResultSet= st.executeQuery(QueryHotel);
			 
            while (ResultSet.next())
            {
            	
        		listaUsuarios.addElement(ResultSet.getString("Usuario"));
        		listaUsuarios.addElement("\n");
        		listaComentarios.addElement(ResultSet.getString("Opinion"));    
        		listaComentarios.addElement("\n");
            	
            }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
///---------------------------------Fin codigo mostrar......................................................		
		// evento para ir a la pagina principal
		
				 MouseListener PaginaPrincipal=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   Pagina_Principal atras=new  Pagina_Principal();
						   atras.setVisible(true);				   }
					};
					label_3.addMouseListener(PaginaPrincipal);
				
			
				
				// evento para ir al espacio personal
						
						 MouseListener EspacioPeresonal=new MouseListener() {
							   
							  public void mouseReleased(MouseEvent arg0) { }	   
							  public void mousePressed(MouseEvent arg0) {  }		   
							  public void mouseExited(MouseEvent arg0) {  }
							  public void mouseEntered(MouseEvent arg0) { }
							  
							 public void mouseClicked(MouseEvent arg0) {
										  
								   EspacioPersonalApp personal=new  EspacioPersonalApp(usuarios,claves);
								   personal.setVisible(true);				   }
							};
							label_4.addMouseListener(EspacioPeresonal);
				
		
		//Mostrar el campo del texto para que el usuario escriba su comentario
		btnComentar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String Estado="si";

				 try {
					 String QueryPais="SELECT * FROM " + "USUARIOS"+" WHERE Estado= '"+Estado+"'";
					Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
					 Statement st=Conexion.createStatement();
					 java.sql.ResultSet ResultSet;
					 ResultSet= st.executeQuery(QueryPais);
					 
					 while (ResultSet.next())
					 {
						 usuarios=ResultSet.getString("usuario");
						 claves=ResultSet.getString("clave");
						 

					 }

} catch (Exception e2) {
// TODO Auto-generated catch block
e2.printStackTrace();
}	
				
				SQL bd=new SQL();
				try {
					bd.SQLConnection("grupo1","root","");
					switch(Opinar)
					{
					case "Opinar_bar":
						bd.insertData_OpinarBar(codes,usuarios,claves,comentar.getText().toString());
						System.out.println("1");

						break;
					case "Opinar_hotel":
						bd.insertData_OpinarHotel(codes,usuarios,claves,comentar.getText().toString());
						System.out.println("2");

						break;
						
					case "Opinar_restuarante":
						bd.insertData_OpinarRestaurante(codes,usuarios,claves,comentar.getText().toString());
						System.out.println("3");

						break;
					case "Opinar_servicios":
						bd.insertData_OpinarServicio(codes,usuarios,claves,comentar.getText().toString());
						System.out.println("4");

						break;
					
					case "Opinar_entretenimiento":
						bd.insertData_OpinarEntre(codes,usuarios,claves,comentar.getText().toString());
						System.out.println("5");

						break;

					}
				
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		
		
		//..........................................evento para cerrar session......................................................
		 MouseListener CambioEstado=new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				  try {
			           
			        	String estado="no";
			        	String Query = "UPDATE USUARIOS SET Estado='"+estado+"'";
			        	
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			             Pagina_Principal principal=new Pagina_Principal();
			             principal.setVisible(true);
			             setVisible(false);
			            
			           // JOptionPane.showMessageDialog(null, "Sesi�n Cerrada");

			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			           // JOptionPane.showMessageDialog(null, "Error a cerrar sesi�n");
			        }  
				 
				 }
			};
			lblCerrarSesion.addMouseListener(CambioEstado);
			
//..........................................FIN evento para cerrar session......................................................

	}
}
