//Assiya Mrabet
package Usuario;

import java.awt.BorderLayout;
import WipAssiya.SQL;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;

public class Cambiar_contrasena_Usuario_App extends JFrame {

	private JPanel contentPane;
	private JPasswordField contraActual;
	public JPasswordField contraNueva;
	private JPasswordField confirmarNueva;
	public String  Usuarios="";
	public String Claves="";
	public String nuevaContra="";
	

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cambiar_contrasena_Usuario_App frame = new Cambiar_contrasena_Usuario_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Cambiar_contrasena_Usuario_App(String Usuario, String Clave) {
		
		this.Usuarios=Usuario;
		this.Claves=Clave;
		setTitle("Cambiar  contrase\u00F1a");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 573, 424);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(42, 44, 364, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setVerticalAlignment(SwingConstants.BOTTOM);
		label_1.setIcon(new ImageIcon(Cambiar_contrasena_Usuario_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(381, -7, 140, 121);
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Cambiar  contrase\u00F1a");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 14));
		lblNewLabel.setBounds(55, 142, 167, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Contrase\u00F1a actual");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(70, 180, 120, 23);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblContraseaNueva = new JLabel("Confirmar la contrase\u00F1a \r\n");
		lblContraseaNueva.setFont(new Font("Arial", Font.PLAIN, 12));
		lblContraseaNueva.setBounds(70, 248, 152, 23);
		contentPane.add(lblContraseaNueva);
		
		JLabel lblContraseaNueva_1 = new JLabel("Contrase\u00F1a nueva");
		lblContraseaNueva_1.setFont(new Font("Arial", Font.PLAIN, 12));
		lblContraseaNueva_1.setBounds(70, 214, 120, 23);
		contentPane.add(lblContraseaNueva_1);
		
		JButton btnNewButton = new JButton("Guardar los cambios");
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 11));
		
		btnNewButton.setBounds(317, 313, 152, 29);
		contentPane.add(btnNewButton);
		
		final JLabel existe = new JLabel("Existe la contrase\u00F1a");
		existe.setForeground(Color.RED);
		existe.setBounds(409, 219, 138, 14);
		contentPane.add(existe);
		existe.setVisible(false);
		
		final JLabel noCoinciden = new JLabel("las contrase\u00F1as no coinciden");
		noCoinciden.setForeground(Color.RED);
		noCoinciden.setBounds(203, 282, 203, 14);
		contentPane.add(noCoinciden);
		noCoinciden.setVisible(false);
		
		final JLabel incorrecte = new JLabel("Incorrecte");
		incorrecte.setForeground(Color.RED);
		incorrecte.setBounds(394, 185, 75, 14);
		contentPane.add(incorrecte);
		
		contraActual = new JPasswordField();
		contraActual.setBounds(232, 182, 152, 20);
		contentPane.add(contraActual);
		
		contraNueva = new JPasswordField();
		contraNueva.setBounds(232, 216, 152, 20);
		contentPane.add(contraNueva);
		
		confirmarNueva = new JPasswordField();
		confirmarNueva.setBounds(232, 250, 152, 20);
		contentPane.add(confirmarNueva);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(Cambiar_contrasena_Usuario_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(55, 313, 68, 44);
		contentPane.add(btnAtras);
		incorrecte.setVisible(false);
		// guardar la nueva contra.
		//antes de guardarla debemos de comprobar que la contras. actual coincide con la k esta en BD 
		//Y comprobar la nueva k no esta en uso
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQL bd=new SQL();
				try {
					bd.SQLConnection("grupo1","root", "");
					
					if(bd.getValuesUsuario_Clave(contraActual.getText().toString())) //comprobamos si el usuario esta registrado
					 {
						if(bd.getValuesUsuario_Clave(contraNueva.getText().toString())) //comprobamos si la nueva contraseņa del usuario esta registrado
						 {
							existe.setVisible(true);
							noCoinciden.setVisible(false);
							incorrecte.setVisible(false);

							contraActual.setText("");
							contraNueva.setText("");
							confirmarNueva.setText("");	
						 }
						else
						{
							 nuevaContra=contraNueva.getText().toString();
							String nueva2=confirmarNueva.getText().toString();
							if(nuevaContra.equals(nueva2)) //combprobar si las contraseņas coinciden
							{
								bd.CambiarContrasenya(nuevaContra,contraActual.getText().toString());
							}
							else
							{
								noCoinciden.setVisible(true);
								existe.setVisible(true);
								incorrecte.setVisible(false);
								contraActual.setText("");
								contraNueva.setText("");
								confirmarNueva.setText("");	
							}
						 }	
				
					 }
					else
						
					{
						incorrecte.setVisible(true);
						existe.setVisible(true);
						noCoinciden.setVisible(false);
						contraActual.setText("");
						contraNueva.setText("");
						confirmarNueva.setText("");						

					 }
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		
//........................................................ evento a clicar sobre label,atras............................
		
		 MouseListener clikLabel=new MouseListener() {
				   
		  public void mouseReleased(MouseEvent arg0) { }	   
		  public void mousePressed(MouseEvent arg0) {  }		   
		  public void mouseExited(MouseEvent arg0) {  }
		  public void mouseEntered(MouseEvent arg0) { }
		  
		 public void mouseClicked(MouseEvent arg0) {
					  
			   EspacioPersonalApp atras=new  EspacioPersonalApp(Usuarios,nuevaContra);
			   atras.setVisible(true);				   }
		};
		   btnAtras.addMouseListener(clikLabel);
		 
//........................................................FIN  evento a clicar sobre label,atras............................

	}
}
