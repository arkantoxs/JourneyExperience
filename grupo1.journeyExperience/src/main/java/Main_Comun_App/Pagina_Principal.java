package Main_Comun_App;

import java.awt.BorderLayout;






import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import Bares.MostrarBar_App;
import Ciudad.MostrarCiudad_App;
import Entretenimiento.MostrarEntre_App;
import Forum.Forum_App;
import Hoteles.MostrarHotel_App;
import Main_Comun_App.Pagina_Principal;
import Pais.Mostrar_Paises_App;
import Restaurantes.MostrarRestauranteApp;
import Servicios.MostrarServiciosApp;
//import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;
import Usuario.Registrar_Usuario_App;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.ListModel;

public class Pagina_Principal extends JFrame {

	private JPanel contentPane;
	private JTextField txtEnEsteEspacio;
	public String usuarios="";
	public String claves="";
	private static Connection Conexion = null;
     SQL bd=new SQL();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pagina_Principal frame = new Pagina_Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public Pagina_Principal() {
		setResizable(false);
		setFont(new Font("Arial", Font.BOLD, 12));
		setBackground(new Color(51, 204, 255));
		setTitle("Pagina Principal");
		setForeground(new Color(51, 153, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 750);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 204, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRestaurantes = new JButton("Restaurantes");
		btnRestaurantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MostrarRestauranteApp ven3= new MostrarRestauranteApp(usuarios,claves);
				ven3.setVisible(true);
				
			}
		});
		
		JButton btnServicios = new JButton("Servicios");
		btnServicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MostrarServiciosApp ven1=new MostrarServiciosApp(usuarios,claves);
				ven1.setVisible(true);
		        
			}
		});
		btnServicios.setBackground(UIManager.getColor("Button.background"));
		btnServicios.setForeground(new Color(0, 0, 0));
		btnServicios.setBounds(412, 116, 89, 23);
		contentPane.add(btnServicios);
		btnRestaurantes.setForeground(new Color(85, 107, 47));
		btnRestaurantes.setBackground(UIManager.getColor("Button.background"));
		btnRestaurantes.setBounds(502, 116, 111, 23);
		contentPane.add(btnRestaurantes);
		
		JButton btnPaises = new JButton("Paises");
		btnPaises.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Mostrar_Paises_App ven5=new Mostrar_Paises_App();
			ven5.setVisible(true);
			
			}
		});
		btnPaises.setBounds(138, 116, 89, 23);
		contentPane.add(btnPaises);
		
		JButton btnCiudades = new JButton("Ciudades");
		btnCiudades.setBackground(UIManager.getColor("Button.background"));
		btnCiudades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MostrarCiudad_App ven6=new MostrarCiudad_App();
				ven6.setVisible(true);
			}
		});
		btnCiudades.setBounds(230, 116, 89, 23);
		contentPane.add(btnCiudades);
		
		JButton btnHoteles = new JButton("Hoteles");
		btnHoteles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MostrarHotel_App ven7=new MostrarHotel_App(usuarios,claves);
				ven7.setVisible(true);
				
			}
		});
		btnHoteles.setForeground(new Color(255, 140, 0));
		btnHoteles.setBackground(UIManager.getColor("Button.background"));
		btnHoteles.setBounds(323, 116, 89, 23);
		contentPane.add(btnHoteles);
		
		JButton btnEntretenimiento = new JButton("Entretenimiento");
		btnEntretenimiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			MostrarEntre_App ven11=new MostrarEntre_App(usuarios,claves);
			ven11.setVisible(true);
			}
		});
		btnEntretenimiento.setForeground(new Color(255, 0, 255));
		btnEntretenimiento.setBackground(UIManager.getColor("Button.background"));
		btnEntretenimiento.setBounds(704, 116, 136, 23);
		contentPane.add(btnEntretenimiento);
		
		JButton btnBares = new JButton("Bares");
		btnBares.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			MostrarBar_App bar=new MostrarBar_App(usuarios,claves);
			bar.setVisible(true);
			}
		});
		btnBares.setForeground(new Color(25, 25, 112));
		btnBares.setBackground(UIManager.getColor("Button.background"));
		btnBares.setBounds(614, 116, 89, 23);
		contentPane.add(btnBares);
		
		JButton btnForum = new JButton("Forum");
		btnForum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Forum_App ven16=new Forum_App(usuarios,claves);	
			ven16.setVisible(true);
			}
		});
		btnForum.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnForum.setForeground(new Color(255, 0, 0));
		//btnForum.setBorder(new CompoundBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 0, 255)), new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 128, 0))));
		btnForum.setBounds(34, 116, 89, 51);
		contentPane.add(btnForum);
		
		JLabel lblBienvenidosA = new JLabel("        Bienvenido");
		lblBienvenidosA.setFont(new Font("Arial Black", Font.BOLD, 56));
		lblBienvenidosA.setForeground(new Color(139, 69, 19));
		lblBienvenidosA.setBounds(109, 258, 770, 63);
		contentPane.add(lblBienvenidosA);
		
		JLabel lblAlEspacioDe = new JLabel("a tu espacio de experiencia\r\n");
		lblAlEspacioDe.setForeground(new Color(139, 69, 19));
		lblAlEspacioDe.setFont(new Font("Arial Black", Font.BOLD, 38));
		lblAlEspacioDe.setBounds(165, 453, 639, 63);
		contentPane.add(lblAlEspacioDe);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(Pagina_Principal.class.getResource("/Vistas/iconoPrincipal.jpg")));
		lblNewLabel_2.setBounds(681, 16, 148, 91);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblJourneyExperience = new JLabel(" Journey & Experience");
		lblJourneyExperience.setForeground(Color.WHITE);
		lblJourneyExperience.setFont(new Font("Arial Black", Font.ITALIC, 35));
		lblJourneyExperience.setBounds(93, 38, 527, 43);
		contentPane.add(lblJourneyExperience);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 11, 835, 100);
		lblNewLabel.setIcon(new ImageIcon(Pagina_Principal.class.getResource("/Vistas/malaspina2010.jpg")));
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(18, 180, 827, 531);
		contentPane.add(lblNewLabel_1);
		lblNewLabel_1.setIcon(new ImageIcon(Pagina_Principal.class.getResource("/Vistas/holbox-1.jpg")));
		
		JLabel lbliniciarSesion = new JLabel("Iniciar Sesion");
		lbliniciarSesion.setFont(new Font("Arial", Font.BOLD, 12));
		lbliniciarSesion.setBounds(658, 150, 89, 18);
		contentPane.add(lbliniciarSesion);
		
		JLabel lblregistrarse = new JLabel("Registrarse");
		lblregistrarse.setFont(new Font("Arial", Font.BOLD, 12));
		lblregistrarse.setBounds(757, 153, 83, 14);
		contentPane.add(lblregistrarse);
		
		txtEnEsteEspacio = new JTextField();
		txtEnEsteEspacio.setBounds(233, 216, 350, 107);
		contentPane.add(txtEnEsteEspacio);
		txtEnEsteEspacio.setText("En este espacio puedes \r\nencontrar opiniones y \r\ncomentarios sobre \r\naspectos de viajes- ");
		txtEnEsteEspacio.setEditable(false);
		txtEnEsteEspacio.setColumns(10);
		
		
//..................................................fin de codigo de editar de eliminar pais.............................................//	

	       
		// ir al espacio personal
		
				// evento para ir al espacio personal
				
				 MouseListener EspacioPeresonal=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   EspacioPersonalApp personal=new  EspacioPersonalApp(usuarios,claves);
						   personal.setVisible(true);				   }
					};
		// cerrar sesion
				
				//..........................................evento para cerrar session......................................................
				 MouseListener CambioEstado=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						  try {
					           
					        	String estado="no";
					        	String Query = "UPDATE USUARIOS SET Estado='"+estado+"'";
					        	
					            Statement st = Conexion.createStatement();
					            st.executeUpdate(Query);

					             Pagina_Principal principal=new Pagina_Principal();
					             principal.setVisible(true);
					             setVisible(false);
					            
					           // JOptionPane.showMessageDialog(null, "Sesi�n Cerrada");

					        } catch (SQLException ex) {
					            System.out.println(ex.getMessage());
					           // JOptionPane.showMessageDialog(null, "Error a cerrar sesi�n");
					        }  
						 
						 }
					};
					
	//..........................................FIN evento para cerrar session......................................................


    
		
		//LISTENER DE LABEL INICIAR SESION
		MouseListener iniSesion =new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   Iniciar_Session_Usuario_App inicio = new Iniciar_Session_Usuario_App();
				   inicio.setVisible(true);
				   
				   }
			};
			lbliniciarSesion.addMouseListener(iniSesion);
			
		//LISTENER DE LABEL REGISTRAR
		MouseListener registrar =new MouseListener() {
				   
			public void mouseReleased(MouseEvent arg0) { }	   
			public void mousePressed(MouseEvent arg0) {  }		   
			public void mouseExited(MouseEvent arg0) {  }
			public void mouseEntered(MouseEvent arg0) { }
				  
			public void mouseClicked(MouseEvent arg0) {
							  
					Registrar_Usuario_App registro = new Registrar_Usuario_App();
					registro.setVisible(true);
					   
					}
			};
			lblregistrarse.addMouseListener(registrar);	
		
		
		
		
	}
	
	 
   
	private static void addPopup(Component component, final JPopupMenu popup) {
		
	}
}
