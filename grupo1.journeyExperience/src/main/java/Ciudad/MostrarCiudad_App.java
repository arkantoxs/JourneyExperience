package Ciudad;


import java.awt.BorderLayout;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;

public class MostrarCiudad_App extends JFrame {

	private JPanel contentPane;
	private JTextField Nombre;
	private JTextField Pais;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	public String usuarios="";
	public String claves="";

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MostrarCiudad_App frame = new MostrarCiudad_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public MostrarCiudad_App() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 515, 534);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(10, 27, 335, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(MostrarCiudad_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(318, 11, 145, 76);
		contentPane.add(label_1);
		
		final JLabel lblNewLabel = new JLabel("Datos de ciudad");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(34, 215, 114, 14);
		contentPane.add(lblNewLabel);
		lblNewLabel.setVisible(false);
		
		final JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(34, 256, 57, 14);
		contentPane.add(lblNombre);
		lblNombre.setVisible(false);
		lblNewLabel.setVisible(false);

		
		Nombre = new JTextField();
		Nombre.setEditable(false);
		Nombre.setBounds(113, 253, 176, 20);
		contentPane.add(Nombre);
		Nombre.setColumns(10);
		Nombre.setVisible(false);
		lblNombre.setVisible(false);
		
		final JLabel lblNewLabel_1 = new JLabel("Pais");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(35, 283, 46, 14);
		contentPane.add(lblNewLabel_1);
		lblNewLabel_1.setVisible(false);
		
		
		final JLabel lblDescripcion = new JLabel("Descripci\u00F3n");
		lblDescripcion.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcion.setBounds(36, 308, 69, 14);
		contentPane.add(lblDescripcion);
		lblDescripcion.setVisible(false);
	
		
		JButton btnGuardar = new JButton("Guardar");
		
		
		btnGuardar.setFont(new Font("Arial", Font.BOLD, 11));
		btnGuardar.setBounds(363, 441, 89, 23);
		contentPane.add(btnGuardar);
		
		final TextArea descripcion = new TextArea();
		descripcion.setEditable(false);
		descripcion.setBounds(113, 304, 267, 114);
		contentPane.add(descripcion);
		descripcion.setVisible(false);
;
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(328, 221, 124, 52);
		contentPane.add(lblNewLabel_2);
		
		Pais = new JTextField();
		Pais.setEditable(false);
		Pais.setColumns(10);
		Pais.setBounds(113, 278, 176, 20);
		contentPane.add(Pais);
		Pais.setVisible(false);
		
		JLabel lblCiudadQueQuieres = new JLabel("Ciudad que quieres ");
		lblCiudadQueQuieres.setFont(new Font("Arial", Font.BOLD, 11));
		lblCiudadQueQuieres.setBounds(37, 163, 111, 14);
		contentPane.add(lblCiudadQueQuieres);
		
		final JComboBox ListaCiudad = new JComboBox();
		ListaCiudad.setBounds(179, 160, 140, 20);
		contentPane.add(ListaCiudad);
	
//..........................codigo para obtener ciudades de BD y a�adirlos a combox...........................................//

		SQL bd=new SQL();

		try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesCiudad();
		
				for(int i=0;i<rellenar.length;i++)
				{
					ListaCiudad.addItem(rellenar[i]);		
				}
		} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
		}

//..........................fin de codigo para obtener CIUDADES y a�adirlos a combox...........................................//
		
		
		JLabel lblNewLabel_3 = new JLabel("mostrar");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_3.setBounds(59, 176, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
	
		btnNewButton.setBounds(363, 159, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel principal = new JLabel("P\u00E1gina principal ");
		principal.setForeground(Color.BLACK);
		principal.setFont(new Font("Arial", Font.BOLD, 13));
		principal.setBounds(40, 109, 124, 23);
		contentPane.add(principal);
		
		JLabel lblForum = new JLabel("      Forum");
		lblForum.setForeground(Color.BLACK);
		lblForum.setFont(new Font("Arial", Font.BOLD, 13));
		lblForum.setBounds(208, 109, 111, 23);
		contentPane.add(lblForum);
		
		JLabel iniciar = new JLabel("Iniciar sesion");
		iniciar.setForeground(Color.BLACK);
		iniciar.setFont(new Font("Arial", Font.BOLD, 13));
		iniciar.setBounds(363, 109, 111, 23);
		contentPane.add(iniciar);

	
	
		
//.................................................... evento a clicar sobre label,atras................................//
		
				 MouseListener clikLabel=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
					
					   setVisible(false);  //CERRAMOS LA VENTANA DE mostrar ciudad

					   Gestion_Ciudad_App atras=new Gestion_Ciudad_App();
					   atras.setVisible(true);				   }
				};
				 
//...........................................fin codigo atras..........................................................//
				 
//.................................... evento para mostrar datos de la ciudad selecionada......................................	
					
				 btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
								
							//SQL bd=new SQL();
							 
							 try {
								 String ciudad= ListaCiudad.getSelectedItem().toString();
								 String QueryPais="SELECT * FROM " + "CIUDADES_PUEBLOS"+ " WHERE NOMBRE= '"+ciudad+"'";
								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryPais);
								 
								 while (ResultSet.next())
								 {
									 Nombre.setText(ResultSet.getString("Nombre"));
									 Pais.setText(ResultSet.getString("Nombre_Pais"));
									 descripcion.setText(ResultSet.getString("Descripcion"));

								 }
								 
		
							// hacer visible los campos para que el admin. modifique los datos de la ciudad selecionada
							descripcion.setVisible(true);
							lblDescripcion.setVisible(true);
							lblNewLabel_1.setVisible(true);
							Nombre.setVisible(true);
							Pais.setVisible(true);
							lblNombre.setVisible(true);
							lblNewLabel.setVisible(true);


						
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							
						

						}
					});
					bd.closeConnection();
//....................................fin  evento para mostrar datos de la ciudad selecionada......................................	

					 //------------------------------------------------- Evento para ir al forum...............................................
					
					 MouseListener Forum=new MouseListener() {
						   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Forum_App PaginaForum=new  Forum_App(usuarios,claves);
							   PaginaForum.setVisible(true);				   }
						};
						lblForum.addMouseListener(Forum);
						    	
			//-------------------------------------------------FIN  Evento para ir al forum...............................................
	
						
						//------------------------------------pagina principal.................................................................
						 
						 MouseListener PaginaPrincipal=new MouseListener() {
								   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Pagina_Principal atras=new  Pagina_Principal();
							   atras.setVisible(true);				   }
						};
						principal.addMouseListener(PaginaPrincipal);
						    	
						   
				//------------------------------------fin ................................................................
						 
						//LISTENER DE LABEL INICIAR SESION
						MouseListener iniSesion =new MouseListener() {
							   
							  public void mouseReleased(MouseEvent arg0) { }	   
							  public void mousePressed(MouseEvent arg0) {  }		   
							  public void mouseExited(MouseEvent arg0) {  }
							  public void mouseEntered(MouseEvent arg0) { }
							  
							 public void mouseClicked(MouseEvent arg0) {
										  
								   Iniciar_Session_Usuario_App inicio = new Iniciar_Session_Usuario_App();
								   inicio.setVisible(true);
								   
								   }
							};
						iniciar.addMouseListener(iniSesion);					
					 
					
					
					
						
	
					
	}
}

