package Ciudad;

import java.awt.BorderLayout;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;

public class AñadirCiudad extends JFrame {

	private JPanel contentPane;
	private JTextField Nombre;
	private static Connection Conexion = null;
	SQL bd=new SQL();

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A�adirCiudad frame = new A�adirCiudad();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public AñadirCiudad() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 494, 453);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(10, 27, 335, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(AñadirCiudad.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(318, 11, 150, 76);
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Datos de ciudad");
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(36, 110, 114, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(new Color(0, 0, 0));
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(46, 159, 57, 14);
		contentPane.add(lblNombre);
		
		Nombre = new JTextField();
		Nombre.setBounds(113, 156, 176, 20);
		contentPane.add(Nombre);
		Nombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Pais");
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(45, 190, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n");
		lblDescripcin.setForeground(new Color(0, 0, 0));
		lblDescripcin.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcin.setBounds(36, 259, 69, 14);
		contentPane.add(lblDescripcin);
		
		JButton btnNewButton_3 = new JButton("Guardar");
		
		btnNewButton_3.setFont(new Font("Arial", Font.BOLD, 11));
		btnNewButton_3.setBounds(345, 341, 89, 23);
		contentPane.add(btnNewButton_3);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(new Color(0, 0, 0));
		btnAtras.setIcon(new ImageIcon(AñadirCiudad.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(23, 352, 68, 51);
		contentPane.add(btnAtras);
		
	
		
		final TextArea descripcion = new TextArea();
		descripcion.setBounds(113, 221, 176, 114);
		contentPane.add(descripcion);
		
		final JComboBox ListaPaises = new JComboBox();
		ListaPaises.setBounds(113, 190, 176, 20);
		contentPane.add(ListaPaises);
		
		final JComboBox ListaAdmin = new JComboBox();
		ListaAdmin.setBounds(328, 108, 121, 20);
		contentPane.add(ListaAdmin);
		
		///......................obtener los nombre de los admin. para identificar quien admin regisro el hotel....................
		
			try {
				bd.SQLConnection("grupo1","root","");
				
			String rellenarAdmin[]=bd.getValues_usuaroAdministrador();
			  
			
			for(int i=0;i<rellenarAdmin.length;i++)
			{
				ListaAdmin.addItem(rellenarAdmin[i]);
				
			}
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	//-------------------------------------------------------FIN CODIGO DE OBTENER admin.........................................		
		
		JLabel Aministrador = new JLabel("Administrador");
		Aministrador.setForeground(new Color(0, 0, 0));
		Aministrador.setFont(new Font("Arial", Font.BOLD, 11));
		Aministrador.setBounds(221, 111, 97, 14);
		contentPane.add(Aministrador);
		
		final JLabel NombreCadena = new JLabel("Valor debe de ser una cadena");
		NombreCadena.setForeground(Color.RED);
		NombreCadena.setFont(new Font("Arial", Font.BOLD, 11));
		NombreCadena.setBounds(299, 156, 169, 20);
		contentPane.add(NombreCadena);
		NombreCadena.setVisible(false);
		
			//SQL bd=new SQL();
		
//........................................ codigo para obtener todos los paises de BD.................................
			try {
				//bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesPais();
			
					for(int i=0;i<rellenar.length;i++)
				{
				ListaPaises.addItem(rellenar[i]);		
				}
				//bd.closeConnection();
		    } catch (Exception e2) {
			// TODO Auto-generated catch block
		    	e2.printStackTrace();
		    }
			
//........................................ FIN codigo para obtener todos los paises de BD.................................
	
		
		// evento a clicar sobre label,atras.
		
				 MouseListener clikLabel=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
					   Gestion_Ciudad_App atras=new Gestion_Ciudad_App();
					   atras.setVisible(true);				   }
				};
				 btnAtras .addMouseListener(clikLabel);
				 
				 
		// evento para guardar los datos
	btnNewButton_3.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
						
			String PaisSeleccionado=ListaPaises.getSelectedItem().toString();

			
			//SQL bd=new SQL();
			try {
				bd.SQLConnection("grupo1", "root", "");
				
		        String dni=bd.Obtener_Dni_Admin(ListaAdmin.getSelectedItem().toString()); //obtener el dni de admin

				if(Nombre.getText().equalsIgnoreCase("")  || descripcion.getText().equalsIgnoreCase(""))
				{
					NombreCadena.setVisible(true);

				}
				else
				{
					bd.insertDataCiudadesPueblos(Nombre.getText(),PaisSeleccionado,descripcion.getText(), dni);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
					        
		}
					});
	bd.closeConnection();
	}
	
}

