/**
 * 
 */
package EntornoGrafico;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author Javier P�rez
 *
 */
public class SQL {
	
	private static Connection Conexion = null;


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
	//Opinar Hotel
		
		public void createTable_Opinarhotel() {
	        try {
	        	
	            String Query = "CREATE TABLE Opinarhotel "
	                    + "(Id_hotel int,Usuario VARCHAR2(20), Clave VARCHAR2(10),"
	                    + "Opinar VARCHAR2(300), data_comentario VARCHAR2(10))";

	            JOptionPane.showMessageDialog(null, "Se ha creado la base de tabla opinarhotel de forma exitosa");
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex); 
	            JOptionPane.showMessageDialog(null, "error " + name);

	        }
	 
	}		
		
		//METODO QUE INSERTA VALORES Opinar_hoteles
		 public void insertData_Opinarhotel( int Id_hotel, String Usuario, String Clave, String Opinar) {
		        try {
		        	
		            String Query = "INSERT INTO Opinarhotel VALUES("
		            		+ "'"+ Id_hotel + "',"
		            		+ "'"+ Usuario + "',"
		            		+ "'"+ Clave + "',"
		            		+ "'"+ Opinar+ "',"
		            		+ "'"+ Fechapc()+ "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 
		//METODO QUE OBTIENE VALORES DE la tabla de Opinar_Hotel	
		 public void getValues_opinarhotel() {
		        try {
		            String Query = "SELECT * FROM Opinarhotel";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                System.out.println("Id_hotel: " + resultSet.getString("Id_hotel") + " "
		                        + "Usuario: " + resultSet.getString("Usuario") + " " 
								+ "Clave: "+ resultSet.getString("Clave") + " "
		                        + "Opinar: " + resultSet.getString("Opinar") + " "
		                        + "data_comentario: " + resultSet.getString("data_comentario"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		    }
		 	
		
			//METODO QUE ELIMINA VALORES de la tabla Opinar_Hotel
		 public void deleteRecord_OpinarHotel( int Id_hotel) {
		        try {
		            String Query = "DELETE FROM Opinarhotel WHERE Id_hotel = '" + Id_hotel + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
	
		
		//METODO QUE CREA TABLA de tener_hotel
			public void createTable_TenerHotel() {
			     try {
			           String Query = "CREATE TABLE TenerHotel"
				                    + "(Id_hotel int,Id_ciudad int)";

			           JOptionPane.showMessageDialog(null, "Se ha creado la tabla TenerHotel de forma exitosa");
						            Statement st = Conexion.createStatement();
						            st.executeUpdate(Query);
						            
				} catch (SQLException ex) {
				          Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
								
		        }
		    }
						
					

			 //METODO QUE INSERTA VALORES de tener hotel
			 public void insertData_TenerHotel( int Id_hotel, int Id_ciudad) {
			        try {
			            String Query = "INSERT INTO  NameTenerHotel VALUES("
				            		+ "'"+ Id_hotel + "',"
				            		+ "'"+ Id_ciudad+ "')";
			            	Statement st = Conexion.createStatement();
			            	st.executeUpdate(Query);
				            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			        } catch (SQLException ex) {
						     JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			        }
			   }	
						 
						 
		//METODO QUE OBTIENE VALORES de tener hotel		
			public void getValues_TenerHotel() {
			      try {
			           String Query = "SELECT * FROM TenerHotel";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);

			            while (resultSet.next()) {
			                System.out.println("Id_hotel: " + resultSet.getString("Id_hotel") + " "
			                + "Id_ciudad: " + resultSet.getString("Id_ciudad"));
			            }

			       } catch (SQLException ex) {
			    	   		JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos hotel");
			  }
			   }
						 
				
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
			 public void deleteRecord_TenerHotel(int Id_hotel, int Id_ciudad) {
		        try {
		            String Query = "DELETE FROM TenerHotel WHERE Id_hotel = '" + Id_hotel + "'"+" and Id_ciudad = '" + Id_ciudad+ "'";
		            	Statement st = Conexion.createStatement();
						st.executeUpdate(Query);

			   } catch (SQLException ex) {
				        System.out.println(ex.getMessage());
						JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
			  }
		   }
					 
			//METODO QUE CREA TABLA de restaurantes
				public void createTable_Restaurante() {
				        try {
				            String Query = "CREATE TABLE Restaurante "
				                    + "(Id_restaurante int, Nombre VARCHAR2(100), contacto int, Especialidad varchar2(200),"
				                    + "Tarifas int, Horario VARCHAR2(50), Descripcion VARCHAR2(200))";

				            JOptionPane.showMessageDialog(null, "Se ha creado la tabla Restaurante de forma exitosa");
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            
				        } catch (SQLException ex) {
				            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
				            JOptionPane.showMessageDialog(null, "error " );
				        }
				    }
				
			 //METODO QUE INSERTA VALORES de Restaurantes
				 public void insertData_Restaurante( int Id_restaurante, String Nombre, int contacto, String Especialidad,int Tarifas,String Horario, String Descripcion) {
				        try {
				            String Query = "INSERT INTO Restaurante VALUES("
				            		+ "'"+ Id_restaurante + "',"
				            		+ "'"+ Nombre + "',"
				            		+ "'"+ contacto + "',"
				            		+ "'"+ Especialidad+ "',"
				            		+ "'"+ Tarifas+ "',"
				            		+ "'"+ Horario+ "',"
				            		+ "'"+ Descripcion+ "')";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
				        }
				    }	
				 
				 
			//METODO QUE OBTIENE VALORES de bares DE NUESTRA BASE DE DATOS		
				 public void getValues_Restaurante() {
				        try {
				            String Query = "SELECT * FROM Restaurantes";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);

				            while (resultSet.next()) {
				                System.out.println("Id_restaurante: " + resultSet.getString("Id_restaurante") + " "
				                        + "Nombre: " + resultSet.getString("Nombre") + " " 
										+ "contacto: "+ resultSet.getString("contacto") + " "
				                        + "Especialidad: " + resultSet.getString("Especialidad") + " "
				                        + "Tarifas: " + resultSet.getString("Tarifas") + " "
						                + "Horario: " + resultSet.getString("Horario") + " "
				                        + "Descripcion: " + resultSet.getString("Descripcion"));
				            }

				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
				        }
				    }
		
		
		
		
				//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
				 public void deleteRecord_Restaurante(int Id_restaurante) {
				        try {
				            String Query = "DELETE FROM Restaurante WHERE Id_restaurante = '" + Id_restautrante + "'";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);

				        } catch (SQLException ex) {
				            System.out.println(ex.getMessage());
				            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
				        }
				    }
				 
				 
				//METODO QUE CREA TABLA de tener_restaurante
					public void createTable_TenerRestaurante() {
					     try {
					           String Query = "CREATE TABLE TenerRestaurante "
						                    + "(Id_restaurante int,Id_ciudad int)";

					           JOptionPane.showMessageDialog(null, "Se ha creado la tabla TenerRestaurante de forma exitosa");
								            Statement st = Conexion.createStatement();
								            st.executeUpdate(Query);
								            
						} catch (SQLException ex) {
						          Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
										
				        }
				    }
								
							

					 //METODO QUE INSERTA VALORES de tener restaurante
					 public void insertData_TenerRestaurante( int Id_restaurante, int Id_ciudad) {
					        try {
					            String Query = "INSERT INTO Restaurante  VALUES("
						            		+ "'"+ Id_restaurante + "',"
						            		+ "'"+ Id_ciudad+ "')";
					            	Statement st = Conexion.createStatement();
					            	st.executeUpdate(Query);
						            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
					        } catch (SQLException ex) {
								     JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
					        }
					   }	
								 
								 
				//METODO QUE OBTIENE VALORES de tener restaurante	
					public void getValues_TenerRestaurante() {
					      try {
					           String Query = "SELECT * FROM TenerRestaurante";
						            Statement st = Conexion.createStatement();
						            java.sql.ResultSet resultSet;
						            resultSet = st.executeQuery(Query);

					            while (resultSet.next()) {
					                System.out.println("Id_restaurante: " + resultSet.getString("Id_restaurante") + " "
					                + "Id_ciudad: " + resultSet.getString("Id_ciudad"));
					            }

					       } catch (SQLException ex) {
					    	   		JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos restaurante");
					  }
					   }
								 
						
				//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
					 public void deleteRecord_TenerRestaurante(int Id_restaurante, int Id_ciudad) {
				        try {
				            String Query = "DELETE FROM TenerHotel WHERE Id_restaurante = '" + Id_restaurante + "'"+" and Id_ciudad = '" + Id_ciudad+ "'";
				            	Statement st = Conexion.createStatement();
								st.executeUpdate(Query);

					   } catch (SQLException ex) {
						        System.out.println(ex.getMessage());
								JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
					  }
				   }		 